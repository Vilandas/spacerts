/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Drone
 */

class ProjectileObject extends MoveableSprite{

    constructor(objectManager, objectData, team, damage, 
        target, startPosition = Vector2.Zero, rotation = 0, splashDamage = 0, splashRadius = 0) {
        let artist = new AnimatedSpriteArtist(objectData);
        artist.SetTake(objectData.defaultTakeName);
    
        let transform = new Transform2D(
            startPosition,
            rotation,
            Vector2.One,
            objectData.origin,
            artist.GetSingleFrameDimensions(objectData.defaultTakeName)
        );
    
        super(
            objectData.id, 
            objectData.actorType, 
            objectData.collisionProperties.type,
            transform, artist,
            objectData.statusType,
            objectData.scrollSpeedMultiplier,
            objectData.layerDepth,
        );
    
        super.CollisionPrimitive = new CircleCollisionPrimitive(transform, objectData.collisionProperties.circleRadius);
        this.objectManager = objectManager;
        this.team = team;
        this.damage = damage;
        this.target = target;
        this.splashDamage = splashDamage;
        this.splashRadius = splashRadius;
    }

    HandleCollisions()
    {
        let collidableActors = [ActorType.Architecture];
        collidableActors = collidableActors.concat(this.team.enemyTargets);

        if(this.CheckOutOfBounds())
        {
            return true;
        }
        for(let i = collidableActors.length - 1; i >= 0; i--)
        {
            let sprites = this.objectManager.Get(collidableActors[i]);
            if(this.CheckCollisions(sprites))
            {
                new Explosion(this.objectManager, this.Transform2D.Translation);
                this.DamageTargetsInRange();
                return true;
            }
        }
        return false;
    }

    CheckCollisions(sprites) 
    {
        for (let i = sprites.length - 1; i >= 0; i--) {
            let sprite = sprites[i];
            if(sprite.CollisionType) {
                if(sprite.ActorType == ActorType.Architecture) {
                    if(Collision.Intersects(this, sprite)) {
                        return true;
                    }
                }
                else if(this.target == null || sprite == this.target) {
                    if(Collision.Intersects(this, sprite)) {
                        NotificationCenter.Notify(new Notification(NotificationType.GameState, 
                            NotificationAction.Health, [sprite, -this.damage]));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    DamageTargetsInRange()
    {
        if(this.splashDamage != 0) {
            let enemyActors = this.team.enemyTargets;
            for(let i = 0; i < enemyActors.length; i++)
            {
                let targets = this.objectManager.Get(enemyActors[i]);
                for(let j = targets.length - 1; j >= 0; j--)
                {
                    if(this.CalculateDistance(
                        this.Transform2D.Translation, targets[j].Transform2D.Translation) <= this.splashRadius
                    )
                    {
                        NotificationCenter.Notify(new Notification(NotificationType.GameState, 
                            NotificationAction.Health, [targets[j], (-this.splashDamage)]));
                    }
                }
            }
        }
    }

    CalculateDistance(pos1, pos2)
    {
        return Math.abs(Vector2.Distance(pos1, pos2));
    }

    CheckOutOfBounds()
    {
        if(this.Transform2D.Translation.X > LEVEL_ARCHITECTURE_DATA.levelLayoutArray[0].length * LEVEL_ARCHITECTURE_DATA.maxBlockWidth ||
            this.Transform2D.Translation.X < 0 ||
            this.Transform2D.Translation.Y > LEVEL_ARCHITECTURE_DATA.levelLayoutArray.length * LEVEL_ARCHITECTURE_DATA.maxBlockHeight ||
            this.Transform2D.Translation.Y < 0
        )
        {
            return true;
        }
        return false;
    }

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}