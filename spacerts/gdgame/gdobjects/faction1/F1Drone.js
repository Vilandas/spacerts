/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Drone
 */

class F1Drone extends UnitObject{

    constructor(objectManager, team, startPosition) {
        
        let objectData = F1DRONE_DATA;
        super(objectManager, objectData, team.type.unit, startPosition);
        
        this.moveBehavior = new UnitRepairMoveBehavior(
            objectManager,
            objectData,
            team
        );
        this.AttachBehavior(this.moveBehavior);
    }

    get MoveBehavior() { return this.moveBehavior; }
}