/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Capital_Tank
 */

class F1Capital_Tank extends UnitObject{

    constructor(objectManager, keyboardManager, notificationCenter, camera, team, startPosition) {

        let objectData = F1CAPITAL_TANK_DATA;
        super(objectManager, objectData, team.type.player, startPosition);
        
        this.coopShipBehavior = new CoopShipBehavior(
            keyboardManager,
            objectManager,
            camera,
            team,
            objectData,
            SMALL_LASER_BULLET_ANIMATION_DATA
        );

        let buildUnitBehavior = new BuildUnitBehavior(keyboardManager, objectManager,
            notificationCenter, team, objectData, startPosition);
        
        this.AttachBehavior(this.coopShipBehavior);
        this.AttachBehavior(buildUnitBehavior);

        objectManager.Add(this); //add sprite
    }

    SetCamera(camera) {        
        this.coopShipBehavior.SetCamera(camera, this);
    }

    RightClick(mouseCoords) {
        //Do nothing
    }
}