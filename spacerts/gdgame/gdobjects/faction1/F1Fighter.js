/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Fighter
 */

class F1Fighter extends UnitObject{

  constructor(objectManager, notificationCenter, team, startPosition, core) {

    let objectData = F1FIGHTER_DATA;
    super(objectManager, objectData, team.type.unit, startPosition);

    this.moveBehavior = new UnitMoveBehavior(
      objectManager,
      objectData,
      team,
      core
    );

    this.shootBehavior = new UnitShootBehavior(
      objectManager,
      notificationCenter,
      team,
      this.moveBehavior,
      objectData,
      SMALL_LASER_BULLET_ANIMATION_DATA
    );

    this.AttachBehavior(this.moveBehavior);
    this.AttachBehavior(this.shootBehavior);
  }

  get MoveBehavior() { return this.moveBehavior; }
  get ShootBehavior() { return this.shootBehavior; }
}