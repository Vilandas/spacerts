/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Buster_Drone
 */

class F1Buster_Drone extends UnitObject{

  constructor(objectManager, team, startPosition, core = null) {
    console.log(team);
    let objectData = F1BUSTER_DRONE_DATA;
    super(objectManager, objectData, team.type.unit, startPosition);

    this.moveBehavior = new UnitExplodeMoveBehavior(
      objectManager,
      objectData,
      team,
      core
    );

    this.AttachBehavior(this.moveBehavior);
  }

  get MoveBehavior() { return this.moveBehavior; }
}