/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class F1Plasma_Ship
 */

class F1Plasma_Ship extends UnitObject{

  constructor(objectManager, notificationCenter, team, startPosition, core) {

    let objectData = F1PLASMA_SHIP_DATA;
    super(objectManager, objectData, team.type.unit, startPosition);

    this.moveBehavior = new UnitMoveBehavior(
      objectManager,
      objectData,
      team,
      core
    );

    this.shootBehavior = new UnitShootBehavior(
      objectManager,
      notificationCenter,
      team,
      this.moveBehavior,
      objectData,
      PLASMA_BULLET_ANIMATION_DATA
    );

    this.AttachBehavior(this.moveBehavior);
    this.AttachBehavior(this.shootBehavior);
  }

  get MoveBehavior() { return this.moveBehavior; }
  get ShootBehavior() { return this.shootBehavior; }
}