/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class HealthBar
 */

class HealthBar extends Sprite{
    //#region  Fields 
    //#endregion 

    //#region  Properties
    //#endregion

    constructor(objectManager, startPosition, diameter, color = Color.Red) {
        let transform = new Transform2D(
            new Vector2(startPosition.X, startPosition.Y + diameter),
            0,
            Vector2.One,
            new Vector2(diameter/2, HEALTHBAR_HEIGHT/2),
            new Vector2(diameter, HEALTHBAR_HEIGHT)
        );
        
        let spriteArtist = new RectangleSpriteArtist(
            1,
            Color.Black,
            color,
            1
        );
        
        super(
            "healthbar",
            ActorType.HUD,
            CollisionType.NotCollidable,
            transform,
            spriteArtist,
            StatusType.IsUpdated | StatusType.IsDrawn,
            1,
            1
        );

        super.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

        this.diameter = diameter;

        objectManager.Add(this);
    }

    //other methods...

    SetBarByFraction(percentage) {
        this.Transform2D.Dimensions = new Vector2((this.diameter * percentage), HEALTHBAR_HEIGHT)
    }

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        throw "Not Yet Implemented";
    }
    //#endregion
}