/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class Explosion
 */

class Explosion extends EffectObject{

    constructor(objectManager, startPosition, scale) {
  
      let objectData = EXPLOSION_DATA;
      super(objectManager, objectData, startPosition, scale);
    }
  }