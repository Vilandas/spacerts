/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class EffectObject
 */

class EffectObject extends Sprite{

    constructor(objectManager, objectData, startPosition, scale = Vector2.One) {
        let artist = new AnimatedSpriteArtist(objectData);
        artist.SetTake(objectData.defaultTakeName);
    
        let transform = new Transform2D(
            startPosition,
            0,
            scale,
            objectData.origin,
            artist.GetSingleFrameDimensions(objectData.defaultTakeName)
        );
    
        super(
            objectData.id + ObjectManager.Count, 
            objectData.actorType, 
            objectData.collisionProperties.type,
            transform, artist,
            objectData.statusType,
            objectData.scrollSpeedMultiplier,
            objectData.layerDepth,
        );
    
        super.CollisionPrimitive = new CircleCollisionPrimitive(transform, objectData.collisionProperties.circleRadius)
        
        objectManager.Add(this);
    }

    //other methods...

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}