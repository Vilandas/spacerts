class Bullet extends Spirte
{
    constructor(ctx, spriteSheet, objectManager)
    {
        let spriteArtist = AnimatedSpriteArtist;
        super("small_laser_bullet",
            ActorType.Bullet,
            new Transform2D(
                BULLET_ALL_INITIAL_TRANSLATION,
                0,
                new Vector2(1, 1),
                Vector2.Zero,
                new Vector2(
                    BULLET_PLAYER_WIDTH,
                    BULLET_PLAYER_HEIGHT
                )
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                BULLET_PLAYER_ANIMATION_FPS,
                BULLET_PLAYER_CELLS,
                0
            ),
            StatusType.Off
        );
        
        this.ctx = ctx;
        this.spriteSheet = spriteSheet;
        this.objectManager = objectManager;

        this.AttachBehavior(
            new MoveBehavior(BULLET_PLAYER_DIRECTION, BULLET_PLAYER_MOVE_SPEED)
        );
    }

    Clone() 
    {
        //make a clone of the actor
        let clone = new Bullet(this.ctx, this.spriteSheet, this.objectManager);

        //now clone all the actors attached behaviors
        for(let behavior of this.behaviors)
            clone.AttachBehavior(behavior.Clone());
        
        //lastly return the actor
        return clone;
    }
}