/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class Core
 */

class Core extends BuildingObject{

    constructor(objectManager, team, startPosition) {
  
      let objectData = CORE_DATA;
      super(objectManager, objectData, team.type.building, startPosition);

      objectManager.Add(this); //add sprite
    }
  }