/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class Selection
 */

class Selection extends Sprite{
    //#region  Fields 
    //#endregion 

    //#region  Properties
    //#endregion

    constructor(copyTransform) {
        let transform = copyTransform || new Transform2D(
            Vector2.Zero,
            0,
            Vector2.One,
            Vector2.Zero,
            Vector2.Zero
        );
        
        let spriteArtist = new RectangleSpriteArtist(
            1,
            "rgb(0, 150, 255)",
            "rgba(0, 0, 255, 0.2)",
            1
        );
        
        super(
            "selection",
            ActorType.HUD,
            CollisionType.Collidable,
            transform,
            spriteArtist,
            StatusType.IsUpdated | StatusType.IsDrawn,
            1,
            1
        );

        super.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);
    }

    //other methods...

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone(objectManager) {
        return new Selection(this.Transform2D.Clone());
    }
    //#endregion
}