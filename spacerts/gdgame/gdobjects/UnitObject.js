/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class UnitObject
 */

class UnitObject extends MoveableSprite{

    constructor(objectManager, objectData, actorType, startPosition) {
        let artist = new AnimatedSpriteArtist(objectData);
        artist.SetTake(objectData.defaultTakeName);
    
        let transform = new Transform2D(
            startPosition,
            0,
            Vector2.One,
            objectData.origin,
            artist.GetSingleFrameDimensions(objectData.defaultTakeName)
        );
    
        super(
            objectData.id + ObjectManager.Count, 
            actorType, 
            objectData.collisionProperties.type,
            transform, artist,
            objectData.statusType,
            objectData.scrollSpeedMultiplier,
            objectData.layerDepth,
        );

        super.Body.MaximumSpeed = objectData.moveProperties.maximumSpeed;
        super.Body.MaximumSpeed = objectData.moveProperties.maximumSpeed;
        super.Body.Friction = objectData.moveProperties.frictionType;
        super.Body.Gravity = objectData.moveProperties.gravityType;
    
        super.CollisionPrimitive = new CircleCollisionPrimitive(transform, objectData.collisionProperties.circleRadius)
        this.lookDirection = objectData.moveProperties.lookDirection.Clone();

        this.moveBehavior;
        this.shootBehavior;

        this.healthBar = new HealthBar(objectManager, startPosition, objectData.collisionProperties.circleRadius*2);

        NotificationCenter.Notify(new Notification(NotificationType.GameState, 
            NotificationAction.Spawn, [this.ID, objectData.shipProperties.maxHealth, this.healthBar]));
    }

    set LookDirection(lookDirection) {
        this.lookDirection = lookDirection;
    }

    get LookDirection() {
        return this.lookDirection;
    }

    get HealthBar() {
        return this.healthBar;
    }
    
    RightClick(mouseCoords, target, userClick = true) {
        this.moveBehavior.RightClick(mouseCoords, this, target);
        if(this.shootBehavior != null)
            this.shootBehavior.RightClick(target, userClick);

    }

    //other methods...

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
    }