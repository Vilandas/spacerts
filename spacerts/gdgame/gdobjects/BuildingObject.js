/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class BuildingObject
 */

class BuildingObject extends Sprite{

    constructor(objectManager, objectData, actorType, startPosition) {
        let artist = new AnimatedSpriteArtist(objectData);
        artist.SetTake(objectData.defaultTakeName);
    
        let transform = new Transform2D(
            startPosition,
            0,
            Vector2.One,
            objectData.origin,
            artist.GetSingleFrameDimensions(objectData.defaultTakeName)
        );
    
        super(
            objectData.id + ObjectManager.Count, 
            actorType, 
            objectData.collisionProperties.type,
            transform, artist,
            objectData.statusType,
            objectData.scrollSpeedMultiplier,
            objectData.layerDepth,
        );
    
        super.CollisionPrimitive = new RectCollisionPrimitive(transform, objectData.collisionProperties.explodeRectangleBy);

        this.healthBar = new HealthBar(objectManager, startPosition, transform.Dimensions.X);

        NotificationCenter.Notify(new Notification(NotificationType.GameState, 
            NotificationAction.Spawn, [this.ID, objectData.buildingProperties.maxHealth, this.healthBar]));
    }

    get HealthBar() {
        return this.healthBar;
    }

    //other methods...

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
    }