  const BACKGROUND_DATA = Object.freeze({
    spriteSheet: document.getElementById("space_nova"),
    sourcePosition: new Vector2(0, 0),
    sourceDimensions: new Vector2(3072, 2112),
    rotationInRadians: 0,
    scale: new Vector2(1, 1),
    origin: new Vector2(0, 0),
    actorType: ActorType.Background,
    statusType: StatusType.IsDrawn,
    scrollSpeedMultiplier: 1,
    layerDepth: 0,
    alpha: 1,
    collisionProperties: {
      type: CollisionType.NotCollidable,
      primitive: CollisionPrimitiveType.Rectangle,
      //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
      circleRadius: 0,
      explodeRectangleBy: 0,
    }
  });

  //#region SPRITE DATA - LEVEL LAYOUT
  
  /*
  id:                         descriptive string for the object, does not have to be unique
  spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
  sourcePosition:             (x,y) texture space co-ordinates of the top-left corner of the sprite data in the spritesheet
  sourceDimensions:           original un-scaled dimensions (w,h) of the sprite data in the spritesheet
  rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
  scale:                      scale to be applied to the drawn sprite about its origin
  origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
  actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
  statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
  scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
  layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
  alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
  collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
  */
  const LEVEL_ARCHITECTURE_DATA = Object.freeze({
    //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
    id: "level architecture data",
    levelSprites: {
      1: { //tile
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(0, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      2: {  //top-left corner wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(64, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      3: { //top horizontal wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(128, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      4: { //top-right corner wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(192, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      5: { //left vertical wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(256, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      6: { //bottom-left corner wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(320, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      7: { //bottom horizontal wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(384, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      8: { //bottom-right corner wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(448, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      9: { //right vertical wall
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(512, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Architecture,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.Collidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      10: { //bottom-left corner shadow
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(576, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      11: { //top horizontal wall shadow
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(640, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      12: { //bottom-right corner shadow
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(704, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      13: { //top-right corner shadow
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(768, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      14: { //right vertical wall shadow
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(832, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      15: { //spawnzone bottom p1
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(896, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      16: { //spawnzone bottom p2
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(960, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      17: { //spawnzone top p1
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(1024, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      18: { //spawnzone top p2
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(1088, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      19: { //spawnzone left p1
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(1024, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      20: { //spawnzone left p2
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(896, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      21: { //spawnzone right p1
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(1088, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      },
      22: { //spawnzone right p2
        spriteSheet: document.getElementById("tileset"),
        sourcePosition: new Vector2(960, 0),
        sourceDimensions: new Vector2(64, 64),
        rotationInRadians: 0,
        scale: new Vector2(1.5, 1.5),
        origin: new Vector2(0, 0),
        actorType: ActorType.Decorator,
        statusType: StatusType.IsDrawn,
        scrollSpeedMultiplier: 1,
        layerDepth: 0,
        alpha: 1,
        collisionProperties: {
          type: CollisionType.NotCollidable,
          primitive: CollisionPrimitiveType.Rectangle,
          //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
          circleRadius: 0,
          explodeRectangleBy: 0,
        }
      }
    },
    
    maxBlockWidth: 96,
    maxBlockHeight: 96, 

    //32*22 = 2048 * 1408
    //(1.5) = 3072 * 2112
    levelLayoutArray: [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 8,17,18, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [2, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 4],
      [5,19, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,21, 9],
      [5,20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,22, 9],
      [6, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 8],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 0],
      [0, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 4,15,16, 2, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 7, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
  });
  
  //#endregion