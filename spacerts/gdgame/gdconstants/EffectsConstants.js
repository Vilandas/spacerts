const EXPLOSION_DATA = Object.freeze({
    id: "explosion",
    spriteSheet: document.getElementById("explosion_spritesheet"),
    defaultTakeName: "idle",
    translation: new Vector2(0, 0),
    rotationInRadians: 0,
    scale: new Vector2(1, 1),
    origin: new Vector2(32, 32),
    actorType: ActorType.NonCollidableAnimatedDecorator,
    statusType: StatusType.IsDrawn | StatusType.IsUpdated,
    scrollSpeedMultiplier: 1,
    layerDepth: 1,
    alpha: 1,
    collisionProperties: {
      type: CollisionType.NotCollidable,
      primitive: CollisionPrimitiveType.Rectangle,
      //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
      circleRadius: 0,
      explodeRectangleBy: 0,
    },
    takes: {  
      "idle" :  {
        fps: 16,
        maxLoopCount: 1, //0 >= always, 1 = run once, N = run N times
        startCellIndex: 0,
        endCellIndex: 4,
        boundingBoxDimensions: new Vector2(64, 64), 
        cellData: [
          new Rect(0, 0, 64, 64),
          new Rect(64, 0, 64, 64),
          new Rect(128, 0, 64, 64),
          new Rect(192, 0, 64, 64),
          new Rect(256, 0, 64, 64)
        ]
      }
    }
  });