const F1CAPITAL_TANK_DATA = Object.freeze({
  id: "f1capital_tank",
  spriteSheet: document.getElementById("f1capital_tank_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(50, 65.5),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 65.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)), //straight-down according to source image
    moveKeys: MOVE_KEYS,
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(2),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  shipProperties: {
    maxHealth: 100
  },
  shootProperties: {
    shootIntervalInMS: 800,
    projectileSpeed: 0.6,
    damage: 5,
    projectileCount: 1,
    projectileOffset: new Vector2(0, 0),
    splashDamage: 0,
    splashRadius: 0,
    shootKeys: {  //add other abilities?
      shoot: Keys.Space
    }
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 131), 
      cellData: [
        new Rect(0, 0, 100, 131) //play frame where player stands repeatedly
      ]
    }
  }
});

const F1CAPITAL_GOLIATH_DATA = Object.freeze({
  id: "f1capital_goliath",
  spriteSheet: document.getElementById("f1capital_goliath_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(105.5, 85),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 105.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)), //straight-down according to source image
    moveKeys: MOVE_KEYS,
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(2),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  shipProperties: {
    maxHealth: 100
  },
  shootProperties: {
    shootIntervalInMS: 1500,
    projectileSpeed: 0.6,
    damage: 2,
    projectileCount: 4,
    projectileOffset: new Vector2(22.5, 0),
    splashDamage: 0,
    splashRadius: 0,
    shootKeys: {  //add other abilities?
      shoot: Keys.Space
    }
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(211, 170), 
      cellData: [
        new Rect(0, 0, 211, 170) //play frame where player stands repeatedly
      ]
    }
  }
});


const F1DRONE_DATA = Object.freeze({
  id: "f1drone",
  spriteSheet: document.getElementById("f1drone_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(23.5, 18.5),
  actorType: ActorType.Unit,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 23.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)),
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(6),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  shipProperties: {
    maxHealth: 6
  },
  shootProperties: {
    shootIntervalInMS: 1000,
    projectileSpeed: 0,
    projectileRange: 150,
    damage: 2,
    projectileCount: 1,
    splashDamage: 0,
    splashRadius: 0,
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(47, 37), 
      cellData: [
        new Rect(0, 0, 47, 37) //play frame where player stands repeatedly
      ]
    },
    "moving" :  {
      fps: 12,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 1,
      boundingBoxDimensions: new Vector2(47, 37), 
      cellData: [
        new Rect(47, 0, 47, 53),
        new Rect(94, 0, 47, 64)
      ]
    }
  }
});

const F1FIGHTER_DATA = Object.freeze({
  id: "f1fighter",
  spriteSheet: document.getElementById("f1fighter_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(33.5, 38.5),
  actorType: ActorType.Unit,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 38.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)),
    moveSpeed: 0.07,
    rotateSpeedInRadians: GDMath.ToRadians(4),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 6,
  },
  shootProperties: {
    shootIntervalInMS: 1000,
    projectileSpeed: 0.6,
    projectileRange: 350,
    damage: 2,
    splashDamage: 0,
    splashRadius: 0,
  },
  shipProperties: {
    maxHealth: 10
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(67, 78), 
      cellData: [
        new Rect(0, 0, 67, 78) //play frame where player stands repeatedly
      ]
    },
    "moving" :  {
      fps: 12,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 1,
      boundingBoxDimensions: new Vector2(67, 78), 
      cellData: [
        new Rect(67, 0, 67, 101),
        new Rect(134, 0, 67, 112)
      ]
    }
  }
});

const F1PLASMA_SHIP_DATA = Object.freeze({
  id: "f1plasma_ship",
  spriteSheet: document.getElementById("f1plasma_ship_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(50.5, 63.5),
  actorType: ActorType.Unit,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 63.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)),
    moveSpeed: 0.07,
    rotateSpeedInRadians: GDMath.ToRadians(4),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 6,
  },
  shootProperties: {
    shootIntervalInMS: 3000,
    projectileSpeed: 0.4,
    projectileRange: 500,
    damage: 5,
    splashDamage: 5,
    splashRadius: 150,
  },
  shipProperties: {
    maxHealth: 20
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(101, 127), 
      cellData: [
        new Rect(0, 0, 101, 127) //play frame where player stands repeatedly
      ]
    },
    "moving" :  {
      fps: 12,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 1,
      boundingBoxDimensions: new Vector2(101, 127), 
      cellData: [
        new Rect(101, 0, 101, 127),
        new Rect(202, 0, 101, 143)
      ]
    },
    "shooting" :  {
      fps: 1,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(101, 127), 
      cellData: [
        new Rect(303, 0, 101, 127),
        new Rect(404, 0, 101, 127),
        new Rect(505, 0, 101, 127),
        new Rect(606, 0, 101, 127)
      ]
    }
  }
});

const F1BUSTER_DRONE_DATA = Object.freeze({
  id: "f1buster_drone",
  spriteSheet: document.getElementById("f1buster_drone_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(48.5, 44),
  actorType: ActorType.Unit,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 48.5,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, -1)),
    moveSpeed: 0.07,
    rotateSpeedInRadians: GDMath.ToRadians(6),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 6,
  },
  shootProperties: {
    shootIntervalInMS: 0,
    projectileSpeed: 0,
    projectileRange: 250,
    damage: 7,
    splashDamage: 5,
    splashRadius: 200,
  },
  shipProperties: {
    maxHealth: 5
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(97, 71), 
      cellData: [
        new Rect(0, 0, 97, 71) //play frame where player stands repeatedly
      ]
    },
    "moving" :  {
      fps: 12,
      maxLoopCount: -1,
      startCellIndex: 0,
      endCellIndex: 1,
      boundingBoxDimensions: new Vector2(97, 71), 
      cellData: [
        new Rect(97, 0, 97, 93),
        new Rect(194, 0, 97, 106)
      ]
    }
  }
});