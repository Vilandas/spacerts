const CORE_DATA = Object.freeze({
  id: "core",
  spriteSheet: document.getElementById("core_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(104, 104),
  actorType: ActorType.Building,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Rectangle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 0,
    explodeRectangleBy: 0,
  },
  buildingProperties: {
    maxHealth: 80
  },
  takes: {  
    "idle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(208, 208), 
      cellData: [
        new Rect(0, 0, 208, 208)
      ]
    }
  }
});