const SMALL_LASER_BULLET_ANIMATION_DATA = Object.freeze({
  id: "small_laser_bullet",
  spriteSheet: document.getElementById("small_laser_bullet_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(3.5, 24),
  actorType: ActorType.Bullet,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.CircleD,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 24,
    explodeRectangleBy: 0,
  },
  takes: {  
    "idle" :  {
      fps: 12,
      leadInDelayMs: 0,
      leadOutDelayMs: 0,
      maxLoopCount: -1, //-1 = always, 0 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(7, 48),
      cellData: [
        new Rect(0, 0, 7, 48)
      ]
    }
  }
});

const PLASMA_BULLET_ANIMATION_DATA = Object.freeze({
  id: "plasma_bullet",
  spriteSheet: document.getElementById("plasma_bullet_spritesheet"),
  defaultTakeName: "idle",
  translation: new Vector2(0, 0),
  rotationInRadians: 0,
  scale: new Vector2(1, 1),
  origin: new Vector2(23.5, 23.5),
  actorType: ActorType.Bullet,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Rectangle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 23.5,
    explodeRectangleBy: 0,
  },
  takes: {  
    "idle" :  {
      fps: 2,
      leadInDelayMs: 0,
      leadOutDelayMs: 0,
      maxLoopCount: -1, //-1 = always, 0 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(47, 47),
      cellData: [
        new Rect(0, 0, 47, 47),
        new Rect(47, 0, 47, 47),
        new Rect(94, 0, 47, 47),
        new Rect(141, 0, 47, 47)
      ]
    }
  }
});