//#region Audio data
//audio - step 2 - create array of cues with same unique IDs as were loaded in HTML file
const audioCueArray = [
  new AudioCue("menu_background", AudioType.Background, 1, 1, true, 0),
  new AudioCue("background", AudioType.Background, 1, 1, true, 0),
  new AudioCue("laser", AudioType.Weapon, 1, 1, false, 0)
];
//#endregion

const SCAN_INTERVAL = 1000;
const INCOME_INTERVAL = 1000;
const INCOME_AMOUNT = 15;

const HEALTHBAR_HEIGHT = 10;

const BASE_SPAWN_TIMER = 12000;
const ZONE_SPAWN_INTERVAL = 1500;
const WAVE_START_BUSTER = 5;
const WAVE_START_PLASMA = 7;

const TEAM = Object.freeze({
  t1: {
    type: {
      player: ActorType.Player, 
      unit: ActorType.Unit,
      building: ActorType.Building
    },
    enemyTargets: [ActorType.Enemy]
  },
  t2: {
    type: {
      unit: ActorType.Enemy,
    },
    enemyTargets: [ActorType.Unit, ActorType.Player, ActorType.Building]
  }
});

const COLLIDABLE_MOVE_ACTORS = [
  ActorType.Unit, ActorType.Enemy, ActorType.Architecture, ActorType.Player, ActorType.Building
];

const GENERAL_KEYS = Object.freeze({
  pause: Keys.P
})

const MOVE_KEYS = Object.freeze({
  up: Keys.W,
  left: Keys.A,
  down: Keys.S,
  right: Keys.D
});

const PRODUCTION_DATA = Object.freeze({
  drone: {
    key: Keys.Z,
    cost: 100,
    buildTime: 4000
  },
  fighter: {
    key: Keys.X,
    cost: 200,
    buildTime: 6000
  },
  buster: {
    key: Keys.C,
    cost: 100,
    buildTime: 2000
  },
  plasma: {
    key: Keys.V,
    cost: 500,
    buildTime: 10000
  }
});

const CAMERA_KEYS = Object.freeze({
  up: Keys.ArrowUp,
  left: Keys.ArrowLeft,
  down: Keys.ArrowDown,
  right: Keys.ArrowRight
  // up: Keys.NumPad8,
  // left: Keys.NumPad4,
  // down: Keys.NumPad2,
  // right: Keys.NumPad6,

  // rotateClockwise: Keys.NumPad1,
  // rotateAntiClockwise: Keys.NumPad3,
  // zoomIn: Keys.NumPad9,
  // zoomOut: Keys.NumPad7,
  // reset: Keys.NumPad5
});

//#region UI Data
const CAMERA_PAN_DISTANCE = 100;

const FontType = Object.freeze({
  //Command & Conquer - battle unit - info overlay
  UnitInformationSmall: "12px Comic Sans MS",
  UnitInformationMedium: "18px Comic Sans MS",
  UnitInformationLarge: "24px Comic Sans MS"
});

//#endregion