class MyMenuManager {

    constructor(id, notificationCenter, keyboardManager, menuID, screens, toggleMenuKey) {
        this.id = id;
        this.notificationCenter = notificationCenter;
        this.keyboardManager = keyboardManager;
        this.menuID = menuID;
        this.screens = screens;
        this.toggleMenuKey = toggleMenuKey;
        this.wave = 0;

        this.RegisterForNotifications();
        this.Initialize();
    }

    //#region Notification Handling
    //handle all GameState type events - see PlayerBehavior::HandleEnemyCollision()
    RegisterForNotifications() {

        this.notificationCenter.Register(
            NotificationType.Menu,
            this,
            this.HandleNotification
        );
    }

    HandleNotification(...argArray) {
        let notification = argArray[0];
        switch (notification.NotificationAction) {
            case NotificationAction.ShowMenuChanged:
                this.HandleShowMenu(notification.NotificationArguments);
                break;
            case NotificationAction.Win:
                this.GameOver(notification.NotificationArguments);
            default:
                break;
        }
    }

    HandleShowMenu(argArray) {
        let statusType = argArray[0];

        //is this notification message for me? (e.g. what happens when one player opens her/his menu and the other does not?)
        if((argArray[1] && argArray[1] === this.id)  || (argArray[2] && argArray[2] === this.id))
        {
            //if we created an event to tell the objectmanager to draw and update then it means we want the game to run i.e. hide the menu
            if (statusType == (StatusType.Off | StatusType.IsDrawn))//pause game
                this.Pause();
            else if(statusType == StatusType.Off) //show menu
                this.ShowMain();
            else
                this.StartGame(); //show game
        }
    }

    //#endregion

    Initialize() {
        this.ShowMain();
        this.InitializeEventListeners();
    }

    StartGame() {
        //hide the <div> with class="menu" for the correct parent (i.e. each menu has a parent id)
        this.Hide("#" + this.menuID + " .menu");
        //show canvas where actual game is drawn
        this.Show("#" + this.menuID + " #" + this.screens[0].canvasID);
        if(this.screens.length == 2)
            this.Show("#" + this.screens[1].canvasID);
    }

    StartCoop() {
        this.screens[0].cvs.width = 1200;
        this.screens[0].cvs.style.width = 1200;
        let element = document.querySelector("#parent-top #canvas1");
        element.style.marginRight = "0";
        element.style.right = "1%";

        NotificationCenter.Notify(
            new Notification(
              NotificationType.Main,
              NotificationAction.Add
            )
        );
        this.Show("#" + this.screens[1].canvasID);
    }

    Pause() {
        if(this.screens.length == 2)
            this.Hide("#" + this.screens[1].canvasID);
        this.Hide("#" + this.menuID + " #" + this.screens[0].canvasID); 
        this.ShowPause();
    }

    GameOver(argArray) {
        this.wave = argArray[0];
        if(this.screens.length == 2)
            this.Hide("#" + this.screens[1].canvasID);
        this.Hide("#" + this.menuID + " #" + this.screens[0].canvasID); 
        this.Hide("#" + this.menuID + " #main.submenu");
        this.Show("#" + this.menuID + " .menu");
        this.Show("#" + this.menuID + " #gameover.submenu");
        let container = document.querySelector("#waves_surv");
        container.innerHTML = "Waves: " + this.wave;
    }

    ShowPause() {
        this.Show("#" + this.menuID + " .menu");
        this.Show("#" + this.menuID + " #pause.submenu");
        this.Hide("#" + this.menuID + " #options.submenu");
        this.Hide("#" + this.menuID + " #main.submenu");
        this.Hide("#" + this.menuID + " #pause_options.submenu");
    }

    ShowMain() {
        //hide canvas where actual game is drawn
        this.Hide("#" + this.menuID + " #" + this.screens[0].canvasID); 
        //show the first menu and hide the others
        this.Show("#" + this.menuID + " #main.submenu");
        this.Hide("#" + this.menuID + " #options.submenu");
        this.Hide("#" + this.menuID + " #leaderboard.submenu");
    }

    ShowOptions(pause) {
        this.Hide("#" + this.menuID + " #main.submenu");
        this.Show("#" + this.menuID + " #options.submenu");
        this.Hide("#" + this.menuID + " #leaderboard.submenu");
        this.Hide("#" + this.menuID + " #volume_controls.submenu");
    }

    ShowPauseOptions() {
        this.Hide("#" + this.menuID + " #pause.submenu");
        this.Hide("#" + this.menuID + " #p_volume_controls.submenu");
        this.Hide("#" + this.menuID + " #volume_controls.submenu");
        this.Show("#" + this.menuID + " #pause_options.submenu");
    }

    ShowVolumeOptions() {
        this.Hide("#" + this.menuID + " #options.submenu");
        this.Show("#" + this.menuID + " #volume_controls.submenu");
    }

    ShowVolumePauseOptions() {
        this.Hide("#" + this.menuID + " #pause_options.submenu");
        this.Show("#" + this.menuID + " #volume_controls.submenu");
        this.Hide("#" + this.menuID + " #volume_controls #volume_controls_back_btn");
        this.Show("#" + this.menuID + " #p_volume_controls.submenu");
    }

    ShowLeaderboard() {
        this.Hide("#" + this.menuID + " #main.submenu");
        this.Hide("#" + this.menuID + " #options.submenu");
        this.Show("#" + this.menuID + " #leaderboard.submenu");
    }

    InitializeEventListeners() {
        document.querySelector("#" + this.menuID + " #play_btn.button").addEventListener("click", event => {
            this.StartGame();

            NotificationCenter.Notify(
                new Notification(
                    NotificationType.Main,
                    NotificationAction.SpawnCapital,
                )
            );

            NotificationCenter.Notify(
                new Notification(
                  NotificationType.Menu,
                  NotificationAction.ShowMenuChanged,
                  [StatusType.IsDrawn | StatusType.IsUpdated, this.id]
                )
            );
        });

        document.querySelector("#" + this.menuID + " #coop_btn.button").addEventListener("click", event => {
            this.StartGame();
            this.StartCoop();

            NotificationCenter.Notify(
                new Notification(
                  NotificationType.Menu,
                  NotificationAction.ShowMenuChanged,
                  [StatusType.IsDrawn | StatusType.IsUpdated, this.id]
                )
            );
        });

        document.querySelector("#" + this.menuID + " #p_resume_btn.button").addEventListener("click", event => {
            this.StartGame();

            NotificationCenter.Notify(
                new Notification(
                  NotificationType.Menu,
                  NotificationAction.ShowMenuChanged,
                  [StatusType.IsDrawn | StatusType.IsUpdated, this.id]
                )
            );
        });

        document.querySelector("#" + this.menuID + " #leaderboard_btn.button").addEventListener("click", event => {
            this.ShowLeaderboard();
        });

        document.querySelector("#" + this.menuID + " #options_btn.button").addEventListener("click", event => {
            this.ShowOptions(false);
        });

        document.querySelector("#" + this.menuID + " #options_back_btn.button").addEventListener("click", event => {
            this.ShowMain();
        });

        document.querySelector("#" + this.menuID + " #volume_controls_btn.button").addEventListener("click", event => {
            this.ShowVolumeOptions();
        });

        document.querySelector("#" + this.menuID + " #volume_controls_back_btn.button").addEventListener("click", event => {
            this.ShowOptions();
        });

        document.querySelector("#" + this.menuID + " #p_volume_controls_btn.button").addEventListener("click", event => {
            this.ShowVolumePauseOptions();
        });

        document.querySelector("#" + this.menuID + " #p_volume_controls_back_btn.button").addEventListener("click", event => {
            this.ShowPauseOptions();
        });

        document.querySelector("#" + this.menuID + " #vol_music.slider").addEventListener("change", event => {
            NotificationCenter.Notify(
                new Notification(
                  NotificationType.Sound,
                  NotificationAction.SetVolumeByTheme,
                  [AudioType.Background, (event.currentTarget.value/100)]
                ));
        });

        document.querySelector("#" + this.menuID + " #vol_effects.slider").addEventListener("change", event => {
            NotificationCenter.Notify(
                new Notification(
                  NotificationType.Sound,
                  NotificationAction.SetVolumeByTheme,
                  [AudioType.Weapon, (event.currentTarget.value/100)]
                ));
        });

        document.querySelector("#" + this.menuID + " #leaderboard_back_btn.button").addEventListener("click", event => {
            this.ShowMain();
        });

        document.querySelector("#" + this.menuID + " #p_resume_btn.button").addEventListener("click", event => {
        });

        document.querySelector("#" + this.menuID + " #p_options_btn.button").addEventListener("click", event => {
            this.ShowPauseOptions();
        });

        document.querySelector("#" + this.menuID + " #p_exit_btn.button").addEventListener("click", event => {
            window.location.reload();
        });

        document.querySelector("#" + this.menuID + " #p_options_back_btn.button").addEventListener("click", event => {
            this.ShowPause();
        });

        document.querySelector("#" + this.menuID + " #gameover_exit_btn.button").addEventListener("click", event => {
            let teamName = document.querySelector("#teamname").value;
            console.log(teamName);

            let data = {
                game: "SpaceRTS",
                team: teamName,
                wave: this.wave
            };

            let options = {
                method: "POST",
                mode: "no-cors",
                body: JSON.stringify(data)
            };

            let gdRest = new GDREST();
            let insert = gdRest.getData("http://127.0.0.1:3000/create", options);

            insert.then(data => {
                NotificationCenter.Notify(
                    new Notification(
                      NotificationType.Main,
                      NotificationAction.Win
                    )
                );
            }).catch(error => {
                window.location.reload();
            });
        });
    }

    Update(gameTime) {

        //to do...add code to listen for toggleMenuKey and show/hide menu
    }

    Toggle(cssSelector) {
        let object = document.querySelector(cssSelector);
        if (object.style.display === 'block') {
            object.style.display = 'none';
        } else {
            object.style.display = 'block';
        }
    }

    Show(cssSelector) {
        document.querySelector(cssSelector).style.display = 'block';
    }

    Hide(cssSelector) {
        document.querySelector(cssSelector).style.display = 'none';
    }
}