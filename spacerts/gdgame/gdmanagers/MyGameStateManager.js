/**
 * This class is responsible for listening, and responding to, notifications 
 * relating to game state changes (e.g. win/lose, fire bullet, play animation over pickup)
 * @author NMCG
 * @version 1.0
 * @class MyGameStateManager
 */

class MyGameStateManager extends GameStateManager {
  //#region  Fields 
  //#endregion 

  constructor(id, statusType, objectManager, cameraManager, notificationCenter, context, screens) {
    super(id);
    this.id = id;
    this.statusType = statusType;
    this.objectManager = objectManager;
    this.cameraManager = cameraManager;
    this.notificationCenter = notificationCenter;
    this.context = context;
    this.screens = screens;
    this.RegisterForNotifications();

    this.selectedUnits = [];
    this.health = {};
    this.income = 0;
    this.timer = new Stopwatch();
    this.spawnTimer = new Stopwatch();
    this.wave = 0;
    this.spawnZones = [
      {
        location: new Vector2(LEVEL_ARCHITECTURE_DATA.levelLayoutArray[0].length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockWidth,
          2 * LEVEL_ARCHITECTURE_DATA.maxBlockHeight),
        queue: [],
        timer: new Stopwatch()
      },
      {
        location: new Vector2(LEVEL_ARCHITECTURE_DATA.levelLayoutArray[0].length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockWidth,
          (LEVEL_ARCHITECTURE_DATA.levelLayoutArray.length - 2) * LEVEL_ARCHITECTURE_DATA.maxBlockHeight),
        queue: [],
        timer: new Stopwatch()
      },
      {
        location: new Vector2(2 * LEVEL_ARCHITECTURE_DATA.maxBlockWidth,
          LEVEL_ARCHITECTURE_DATA.levelLayoutArray.length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockHeight),
        queue: [],
        timer: new Stopwatch()
      },
      {
        location: new Vector2((LEVEL_ARCHITECTURE_DATA.levelLayoutArray[0].length - 2) * LEVEL_ARCHITECTURE_DATA.maxBlockWidth,
          LEVEL_ARCHITECTURE_DATA.levelLayoutArray.length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockHeight),
        queue: [],
        timer: new Stopwatch()
      }
    ]
    //setup the UIs for the players listed in screenObjectArray
    this.InitializeAllUIs();

    //this.HandleHealthStateChange([enemy, 0, true]);
    this.core = new Core(this.objectManager, TEAM.t1, new Vector2(
      LEVEL_ARCHITECTURE_DATA.levelLayoutArray[0].length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockWidth,
      LEVEL_ARCHITECTURE_DATA.levelLayoutArray.length/2 * LEVEL_ARCHITECTURE_DATA.maxBlockHeight));

    let enemy = new F1Fighter(this.objectManager, this.notificationCenter, TEAM.t2, new Vector2(200, 1200), this.core);
    this.objectManager.Add(enemy);
  }

  /**
   * Setup all the UIs for the players in the game
   *
   * @memberof MyGameStateManager
   */
  InitializeAllUIs() {
    for (let screen of this.screens)
      this.InitializeUI(screen.uiID);
  }


  /**
   * Creates a UI for each player in the screens and adds the HTML elements (e.g. div_score) to the correct canvas
   * You should change this method to add whatever UI elements your game uses.
   * @param {*} uiID
   * @memberof MyGameStateManager
   */
  InitializeUI(uiID) {
    //get a handle to the div that we use for adding UI elements for this particular canvas (e.g. player-ui-top)
    let container = document.querySelector("#" + uiID);
    //create element to store the income
    let div_income = document.createElement("div");
    let div_wave = document.createElement("div");
    //set text
    div_income.innerText = "Income: " + this.income;
    div_wave.innerText = "Wave: " + this.wave;
    //now class for any effects e.g. fadein
    div_income.setAttribute("id", "income");
    div_income.setAttribute("class", "ui-info");
    div_wave.setAttribute("id", "wave");
    div_wave.setAttribute("class", "ui-info");
    //now position
    div_income.setAttribute("style", "top: 1%; left: 1%; display: none;");
    div_wave.setAttribute("style", "top: 1%; right: 1%; display: none; width: 200px;");
    //add to the container so that we actually see it on screen!
    container.appendChild(div_income);
    container.appendChild(div_wave);
  }

  UpdateUIDisplay() {
    let container = document.querySelector("#" + this.screens[0].uiID);
    let income_div = document.querySelector("#income");
    container.removeChild(income_div);

    container = document.querySelector("#" + this.screens[1].uiID);
    container.appendChild(income_div);

    let info_div = document.querySelector("#bottom-info-ui");
    info_div.setAttribute("style", "left: 1%; margin-left: 0;");
  }

  UpdateUIInfo() {
    let spawnTime = Math.floor(((BASE_SPAWN_TIMER - this.spawnTimer.GetElapsedTime())/1000));
    if(spawnTime < 0)
      spawnTime = 0;
    let div_wave = document.querySelector("#wave");
    div_wave.innerText = "Wave: " + this.wave + "\nNext wave in: " + spawnTime;
    let div_income = document.querySelector("#income");
    div_income.innerText = "Income: " + this.income;
  }

  //#region Notification Handling
  RegisterForNotifications() {

    //register for menu notification
    this.notificationCenter.Register(
        NotificationType.Menu,
        this,
        this.HandleNotification
    );

    //register for pickup notifications
    this.notificationCenter.Register(
        NotificationType.GameState,
        this,
        this.HandleNotification
    );

    this.notificationCenter.Register(
      NotificationType.Main,
      this,
      this.HandleNotification
  );
  }

  HandleNotification(...argArray) {
    let notification = argArray[0];
    switch (notification.NotificationAction) {
      case NotificationAction.ShowMenuChanged:
        this.HandleShowMenuChanged(notification.NotificationArguments);
        break;

      case NotificationAction.Get:
        this.HandleGetCurrency(notification.NotificationArguments);
        break;

      case NotificationAction.Pickup:
        this.HandlePickup(notification.NotificationArguments);
        break;

      case NotificationAction.Health:
        this.HandleHealthStateChange(notification.NotificationArguments);  
        break;

      case NotificationAction.Spawn:
        this.HandleSpawn(notification.NotificationArguments);  
        break;

      case NotificationAction.Inventory:
        this.HandleInventoryStateChange(notification.NotificationArguments);  
        break;

      case NotificationAction.Ammo:
        this.HandleAmmoStateChange(notification.NotificationArguments);  
        break;

      case NotificationAction.AddOneSelection:
        this.HandleUnitSelection(notification.NotificationArguments);
        break;

      case NotificationAction.ClearSelection:
        this.HandleClearSelection();
        break;

      case NotificationAction.RemoveOneSelection:
        this.HandleRemoveOneSelection(notification.NotificationArguments);
        break;

      case NotificationAction.Add:
        this.UpdateUIDisplay();
        break;
      
      case NotificationAction.SpawnCapital:
        break;

      default:
        break;  
    }
  }
  //#endregion

  HideUI() {
    document.querySelector("#income").setAttribute("style", "display: none;");
    document.querySelector("#wave").setAttribute("style", "display: none;");
    document.querySelector("#bottom-info-ui").setAttribute("style", "display: none;");
  }

  HandleShowMenuChanged(argArray) {
    this.statusType = argArray[0];
    if(this.statusType == (StatusType.Off | StatusType.IsDrawn)) {
      this.PlayMenuBackground();
      this.timer.Pause();
      this.spawnTimer.Pause();
      for(let zoneNum in this.spawnZones) {
        if(this.spawnZones[zoneNum].timer.IsPaused == false)
          this.spawnZones[zoneNum].timer.Pause();
      }
      this.HideUI();
    }
    else if(this.statusType == (StatusType.IsDrawn | StatusType.IsUpdated)) {
      if(this.timer.IsRunning) {
        this.PlayGameBackground();
        this.timer.Unpause();
        this.spawnTimer.Unpause();
        for(let zoneNum in this.spawnZones) {
          if(this.spawnZones[zoneNum].timer.IsPaused)
            this.spawnZones[zoneNum].timer.Unpause();
        }
        NotificationCenter.Notify(new Notification(NotificationType.Main, 
          NotificationAction.Resume, [false]));

      }
      else {
        this.timer.Start();
        this.spawnTimer.Start();
        NotificationCenter.Notify(new Notification(NotificationType.Main, 
          NotificationAction.Resume, [false]));
        this.PlayGameBackground();
      }
      document.querySelector("#income").setAttribute("style", "top: 1%; left: 1%; display: block;");
      document.querySelector("#wave").setAttribute("style", "top: 1%; right: 1%; display: block; width: 200px");
      document.querySelector("#bottom-info-ui").setAttribute("style", "display: block;");
      if(this.screens.length == 1)
        document.querySelector("#bottom-info-ui").setAttribute("style", "display: block;");
      else document.querySelector("#bottom-info-ui").setAttribute("style", "left: 1%; margin-left: 0; display: block;");
    }
  }

  PlayGameBackground() {
    NotificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Pause, ["menu_background"]));
    NotificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Play, ["background"]));
  }

  PlayMenuBackground() {
    NotificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Pause, ["background"]));
    NotificationCenter.Notify(new Notification(
      NotificationType.Sound, NotificationAction.Play, ["menu_background"]));
  }

  HandleGetCurrency(argArray) {
    let valid = this.income - argArray[0] >= 0;
    if(valid)
      this.income -= argArray[0];
    NotificationCenter.Notify(new Notification(NotificationType.Currency, 
      NotificationAction.Get, [valid]));
  }
  
  HandlePickup(argArray) {
    //show an animation (based on archetype provided to InitializeArchetypes())
    //                               decoratorID, sprite
    this.ShowPickupAnimatedDecorator(argArray[1], argArray[3]);
    //update the game state (value, parent)
    this.UpdateGameState(argArray[0], argArray[2]);
    //update the UI (value, parent)
    this.UpdateUI(argArray[0], argArray[2]);
  }
  
  ShowPickupAnimatedDecorator(decoratorID, sprite){
    let decorator = this.archetypeArray[decoratorID].Clone();
    decorator.statusType = StatusType.IsDrawn | StatusType.IsUpdated;
    decorator.Transform2D.SetTranslation(Vector2.Subtract(sprite.transform2D.translation, sprite.transform2D.origin));
    this.objectManager.Add(decorator);
  }

  UpdateGameState(value, parent){
  }

  UpdateUI() {
    this.UpdateUIInfo();
  }


  SetArchetypeArray(archetypeArray){
      this.archetypeArray = archetypeArray;
  }

  //[0] = id, [1] = health, [2] = healthbar
  HandleSpawn(argArray){
    this.health[argArray[0]] = {
      maxHealth: argArray[1],
      health: argArray[1],
      healthBar: argArray[2]
    };
  }

  HandleHealthStateChange(argArray){
    if(this.health[argArray[0].ID].health + argArray[1] <= this.health[argArray[0].ID].maxHealth)
    {
      this.health[argArray[0].ID].health += argArray[1];
    }
    if(this.health[argArray[0].ID].health <= 0 || argArray[2])
    {
      if(argArray[0] == this.core) {
        this.GameOver();
      }
      delete this.health[argArray[0].ID];
      NotificationCenter.Notify(new Notification(NotificationType.Target, 
        NotificationAction.Destroy, [argArray[0]]));
      NotificationCenter.Notify(new Notification(NotificationType.Sprite, 
        NotificationAction.RemoveFirst, [argArray[0].HealthBar]));
      NotificationCenter.Notify(new Notification(NotificationType.Sprite, 
          NotificationAction.RemoveFirst, [argArray[0]]));
    }
    else
    {
      let fraction = this.health[argArray[0].ID].health/this.health[argArray[0].ID].maxHealth;
      this.health[argArray[0].ID].healthBar.SetBarByFraction(fraction);
    }
  }

  GameOver() {
    this.HideUI();
    this.timer.Pause();
    this.objectManager.RemoveAllByType(ActorType.Enemy);
    this.objectManager.RemoveAllByType(ActorType.Unit);
    if(this.screens.length == 2) {
      NotificationCenter.Notify(new Notification(NotificationType.Menu,
        NotificationAction.Win, [this.wave, StatusType.Off | StatusType.IsDrawn, this.screens[0].canvasID, this.screens[1].canvasID]));
    }
    else {
      NotificationCenter.Notify(new Notification(NotificationType.Menu,
        NotificationAction.Win, [this.wave, StatusType.Off | StatusType.IsDrawn, this.screens[0].canvasID]));
    }
  }

  HandleInventoryStateChange(argArray){
    console.log(argArray);
    //add your code here..maybe update a UI, or a health variable?
  }

  HandleAmmoStateChange(argArray){
    console.log(argArray);
    //add your code here..maybe update a UI, or a health variable?
  }

  HandleUnitSelection(argArray){
    if(!this.selectedUnits.includes(argArray[0]))
      this.selectedUnits.push(argArray[0]);
  }

  HandleClearSelection(){
    this.selectedUnits.splice(0, this.selectedUnits.length);
  }

  HandleRemoveOneSelection(argArray){
    if(this.selectedUnits.includes(argArray[0]))
    {
      this.selectedUnits.splice(this.selectedUnits.indexOf(argArray[0], 1));
    }
  }

  RightClick(mouseCoords) {
    let target = null;

    target = this.GetTarget(mouseCoords);

    for(let i = 0; i < this.selectedUnits.length; i++)
    {
      this.selectedUnits[i].RightClick(mouseCoords, target);
    }
  }

  GetTarget(mouseCoords) {
    let team = TEAM.t1;
    let target = null;
    for(let enemyType in team.enemyTargets) {
      target = this.GetPotentialTarget(enemyType, team.enemyTargets, mouseCoords);
      if(target != null)
        return target;
    }
    if(target == null) {
      for(let friendType in team.type) {
        target = this.GetPotentialTarget(friendType, team.type, mouseCoords);
        if(target != null)
          return target;
      }
    }
    return null;
  }

  GetPotentialTarget(actorType, teamTarget, mouseCoords) {
    let potentialTargets = this.objectManager.Get(teamTarget[actorType]);
    for(let i = 0; i < potentialTargets.length; i++)
    {
      if(Collision.ContainsPoint(potentialTargets[i], mouseCoords))
      {
        return potentialTargets[i];
      }
    }
    return null;
  }

  Draw(gameTime)
  {
    for(let i = 0; i < this.selectedUnits.length; i++)
    {
      this.DrawSpriteCollisionPrimitive(this.selectedUnits[i].CollisionPrimitive);
    }
  }

  DrawBoundingBox(transform, color) {
    this.context.save();
    this.cameraManager.ActiveCamera.SetContext(this.context);
    this.context.globalAlpha = 1;
    this.context.lineWidth = 1;
    this.context.strokeStyle = color;
    let bb = transform.BoundingBox;
    this.context.strokeRect(
      bb.X,
      bb.Y,
      bb.Width,
      bb.Height
    );
    this.context.restore();
  }

  DrawSpriteCollisionPrimitive(collisionPrimitive) {
    this.cameraManager.ActiveCamera.context.save();
    this.cameraManager.ActiveCamera.SetContext(this.context);
    collisionPrimitive.DebugDraw(this.cameraManager.ActiveCamera.context, 2, 1, Color.CornFlowerBlue);
    this.cameraManager.ActiveCamera.context.restore();
  }


  SpawnController() {
    if(!this.spawnTimer.IsPaused && (this.spawnTimer.GetElapsedTime() >= BASE_SPAWN_TIMER)) {
      this.spawnTimer.Pause();
      this.wave++;

      for(let i = 0; i < this.wave; i++) {
        let random = Math.floor(Math.random() * 4);
        let zone = this.spawnZones[random];
        if(i != 0) {
          if(i % WAVE_START_BUSTER == 0) {
            zone.queue.push(new F1Buster_Drone(this.objectManager,
              TEAM.t2, zone.location.Clone(), this.core));
          }
          else if(i % WAVE_START_PLASMA == 0) {
            zone.queue.push(new F1Plasma_Ship(this.objectManager, this.notificationCenter,
              TEAM.t2, zone.location.Clone(), this.core));
          }
          else {
            zone.queue.push(new F1Fighter(this.objectManager, this.notificationCenter,
              TEAM.t2, zone.location.Clone(), this.core));
          }
        }
        else {
          zone.queue.push(new F1Fighter(this.objectManager, this.notificationCenter,
            TEAM.t2, zone.location.Clone(), this.core));
        }
        for(let zoneNum in this.spawnZones)
          this.spawnZones[zoneNum].timer.Start();
      }
    }
    else if((!this.timer.IsPaused && this.timer.IsRunning) && this.spawnTimer.IsPaused && this.spawnTimer.GetElapsedTime() >= BASE_SPAWN_TIMER) {
      let finishedSpawning = true;
      for(let zoneNum in this.spawnZones) {
        let zone = this.spawnZones[zoneNum];
        if(zone.timer.GetElapsedTime() >= ZONE_SPAWN_INTERVAL && zone.queue.length != 0) {
          this.objectManager.Add(zone.queue[0]);
          zone.queue.splice(0, 1);
          zone.timer.Start();
        }
        if(finishedSpawning && zone.queue.length != 0)
          finishedSpawning = false;
      }
      
      if(finishedSpawning) {
        for(let zoneNum in this.spawnZones) {
          let zone = this.spawnZones[zoneNum];
          zone.timer.Stop();
          zone.timer.Reset();
          this.spawnTimer.Stop(); 
          this.spawnTimer.Reset();
          this.spawnTimer.Start();
        }
      }
    }
  }

  Update(gameTime) {
    if(!this.timer.IsPaused && this.timer.GetElapsedTime() >= INCOME_INTERVAL) {
      this.income += INCOME_AMOUNT;
      this.timer.Start();
    }
    this.UpdateUI();
    this.SpawnController();
  }

  //#region Equals, Clone, ToString 
  Equals(other) {
      //to do...  
      throw "Not Yet Implemented";
  }

  ToString() {
      //to do...
      throw "Not Yet Implemented";
  }

  Clone() {
      //to do...
      throw "Not Yet Implemented";

  }
  //#endregion
}