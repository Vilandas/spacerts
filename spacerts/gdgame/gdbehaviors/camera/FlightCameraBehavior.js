/**
 * Moves Camera2D by changing the parent transform based on time or user input.
 * @author
 * @version 1.0
 * @class FlightCameraBehavior
 */
class FlightCameraBehavior {
  constructor(keyboardManager, moveKeys, moveSpeed, rotateSpeedInRadians, scaleSpeed, cvs, parent) {
    this.keyboardManager = keyboardManager;
    this.moveKeys = moveKeys;
    this.moveSpeed = moveSpeed;
    this.rotateSpeedInRadians = rotateSpeedInRadians;
    this.scaleSpeed = scaleSpeed;
    this.cvs = cvs;
    this.parent = parent;
    this.panArea = {up: false, left: false, down: false, right: false};
    this.AddEventListeners(this.cvs);
  }

  AddEventListeners(cvs)
  {
    cvs.addEventListener("mousemove", (e) => this.BorderPan(e, this));
    cvs.addEventListener("mouseleave", (e) => this.PanOff(e, this));
  }

  PanOff(e, cameraBehavior)
  {
    for(let area in cameraBehavior.panArea)
      area = false;
  }

  BorderPan(e, cameraBehavior)
  {
    let mouse = cameraBehavior.GetMouseCoords(e.clientX, e.clientY);
    if(mouse.X <= CAMERA_PAN_DISTANCE)
    {
      cameraBehavior.panArea.left = true;
    }
    else if(mouse.X >= cameraBehavior.cvs.getBoundingClientRect().width - CAMERA_PAN_DISTANCE)
    {
      cameraBehavior.panArea.right = true;
    }
    else
    {
      cameraBehavior.panArea.left = false;
      cameraBehavior.panArea.right = false;
    }

    if(mouse.Y <= CAMERA_PAN_DISTANCE)
    {
      cameraBehavior.panArea.up = true;
    }
    else if(mouse.Y >= cameraBehavior.cvs.getBoundingClientRect().height - CAMERA_PAN_DISTANCE)
    {
      cameraBehavior.panArea.down = true;
    }
    else
    {
      cameraBehavior.panArea.up = false;
      cameraBehavior.panArea.down = false;
    }
  }

  GetMouseCoords(x, y)
  {
    return new Vector2(
      x - this.cvs.getBoundingClientRect().x,
      y - this.cvs.getBoundingClientRect().y
    );
  }

  CheckOutOfBounds(parent, translateBy) {
    let newLocation = Vector2.Add(parent.Transform2D.Translation, translateBy);
    if(newLocation.X < 0 || newLocation.X > (3072 - this.cvs.width))
      return false;
    if(newLocation.Y < 0 || newLocation.Y > (2112 - this.cvs.height))
      return false;
    return true;
  }

  Execute(gameTime, parent) {

    //translate camera
    let translateBy = null;
    if (this.keyboardManager.IsKeyDown(this.moveKeys.left) || this.panArea.left) {
       translateBy = Vector2.MultiplyScalar(this.moveSpeed, -1);
    }
    else if (this.keyboardManager.IsKeyDown(this.moveKeys.right) || this.panArea.right) {
      translateBy = this.moveSpeed;
    }

    if (this.keyboardManager.IsKeyDown(this.moveKeys.up) || this.panArea.up) {
      translateBy = Vector2.MultiplyScalar(new Vector2(this.moveSpeed.Y, this.moveSpeed.X), -1);
    } 
    else if (this.keyboardManager.IsKeyDown(this.moveKeys.down) || this.panArea.down) {
      translateBy = new Vector2(this.moveSpeed.Y, this.moveSpeed.X);
    }

    if(translateBy != null && this.CheckOutOfBounds(parent, translateBy)) {
      parent.Transform2D.TranslateBy(translateBy);
    }

    // //rotate camera
    // if (this.keyboardManager.IsKeyDown(this.moveKeys.rotateAntiClockwise)) {
    //     parent.Transform2D.RotateBy(-this.rotateSpeedInRadians);
    // } else if (this.keyboardManager.IsKeyDown(this.moveKeys.rotateClockwise)) {
    //     parent.Transform2D.RotateBy(this.rotateSpeedInRadians);
    // }

    // //scale camera
    // if (this.keyboardManager.IsKeyDown(this.moveKeys.zoomIn)) {
    //     parent.Transform2D.ScaleBy(this.scaleSpeed);
    // } else if (this.keyboardManager.IsKeyDown(this.moveKeys.zoomOut)) {
    //     parent.Transform2D.ScaleBy(Vector2.MultiplyScalar(this.scaleSpeed, -1));
    // }

    // //reset camera
    // if (this.keyboardManager.IsKeyDown(this.moveKeys.reset)) {
    //     parent.Transform2D.Reset();
    // }
  }

  /*
    Equals(other) {
        //to do...  
        return false;
    }

    ToString()
    {
        //to do...
        return "undefined";
    }
  

  Clone() {
    return new MoveBehavior(this.moveDirection, this.moveSpeed);
  }
  */
}
