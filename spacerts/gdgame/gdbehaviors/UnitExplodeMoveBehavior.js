/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class UnitExplodeMoveBehavior
 */

class UnitExplodeMoveBehavior {
    //#region  Fields 
    //#endregion 

    //#region  Properties
    //#endregion

    constructor(objectManager, objectData, team, core)
    {
        this.id = "unitExplodeMoveBehavior"; 
        this.objectManager = objectManager;
        this.team = team;
        this.core = core;
        this.originalDirection = objectData.moveProperties.lookDirection.Clone();
        this.moveSpeed = objectData.moveProperties.moveSpeed;
        this.rotateSpeed = objectData.moveProperties.rotateSpeedInRadians;
        this.range = objectData.shootProperties.projectileRange;
        this.damage = objectData.shootProperties.damage;
        this.splashRadius = objectData.shootProperties.splashRadius;
        this.splashDamage = objectData.shootProperties.splashDamage;

        this.turnAngle;
        this.goToPos = Vector2.Zero;
        this.finished = true;
        this.target = null;
        this.distance;
        this.shortestDistance;
        this.timeSinceLastScan = 0;
    }
    
    IsFinishedTurning() {
        return this.turnAngle <= this.rotateSpeed && this.turnAngle >= -this.rotateSpeed;
    }

    IsInRange() {
        if(this.target == null)
            return false;
        if(this.distance <= this.range)
            return true;
        else return false;
    }

    CalculateDistance(pos1, pos2)
    {
        return Math.abs(Vector2.Distance(pos1, pos2));
    }

    ResetTarget()
    {
        this.target = null;
    }

    UpdateLookDirection(parent)
    {
        parent.LookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians)));
    }

    get GoToPos() {
        return this.goToPos; 
    }
    get Finished() {
        return this.finished;
    }
    get Distance() {
        return this.distance;
    }

    HandleMovement(gameTime, parent)
    {
        if(this.turnAngle > this.rotateSpeed)
        {
            parent.Transform2D.RotateBy(this.rotateSpeed);
            this.turnAngle -= this.rotateSpeed;
            this.UpdateLookDirection(parent);
        }
        else if(this.turnAngle < -this.rotateSpeed)
        {
            parent.Transform2D.RotateBy(-this.rotateSpeed);
            this.turnAngle += this.rotateSpeed;
            this.UpdateLookDirection(parent);
        }
        else if(!this.finished)
        {
            //move forward using the look direction
            let translateBy = Vector2.MultiplyScalar(parent.LookDirection, gameTime.ElapsedTimeInMs * this.moveSpeed);
            parent.Body.AddVelocity(translateBy);

            parent.Artist.SetTake("moving");
            let parentPos = parent.Transform2D.Translation;
            let a = this.goToPos.X - (parentPos.X + (2*translateBy.X));
            let b = this.goToPos.Y - (parentPos.Y + (2*translateBy.Y));
            if(Math.sqrt( a*a + b*b ) < parent.CollisionPrimitive.radius)
            {
                this.finished = true;
            }
        }
        else parent.Artist.SetTake("idle");
    }

    ApplyForces(parent) {
        //notice we need to slow body in X and Y and we dont ApplyGravity() in a top-down game
        parent.Body.ApplyFrictionX();
        parent.Body.ApplyFrictionY();
    }

    HandleCollisions(parent)
    {
        for(let i = 0; i < COLLIDABLE_MOVE_ACTORS.length; i++)
        {
            let sprites = this.objectManager.Get(COLLIDABLE_MOVE_ACTORS[i]);
            if(i < 2)
                this.CheckCollisions(parent, sprites, true);
            else this.CheckCollisions(parent, sprites, false);
        }
    }

    CheckCollisions(parent, sprites, boolUnit) 
    {
        for (let i = 0; i < sprites.length; i++) {
            let sprite = sprites[i];
            if(sprite.CollisionType) {
                let collisionLocationType = Collision.GetIntersectsLocation(parent, sprite);
                let collision = false;

                //the code below fixes a bug which caused sprites to stick inside an object
                if (collisionLocationType === CollisionLocationType.Left) {
                    if (parent.Body.velocityX <= 0) {
                        parent.Body.SetVelocityX(0);
                        collision = true;
                    }
                }
                else if (collisionLocationType === CollisionLocationType.Right) {
                    if (parent.Body.velocityX >= 0) {
                        parent.Body.SetVelocityX(0);
                        collision = true;
                    }
                }
                //the code below fixes a bug which caused sprites to stick inside an object
                if (collisionLocationType === CollisionLocationType.Top) {
                    if (parent.Body.velocityY <= 0) {
                        parent.Body.SetVelocityY(0);
                        collision = true;
                    }
                }
                else if (collisionLocationType === CollisionLocationType.Bottom) {
                    if (parent.Body.velocityY >= 0) {
                        parent.Body.SetVelocityY(0);
                        collision = true;
                    }
                }
                if(collision && boolUnit) {
                    if(this.CheckSquadPos(sprite))
                        this.finished = true;
                }
                if(collision && sprite == this.target)
                {
                    new Explosion(this.objectManager, parent.Transform2D.Translation, new Vector2(2, 2));
                    NotificationCenter.Notify(new Notification(NotificationType.GameState, 
                        NotificationAction.Health, [this.target, -this.damage]));
                    this.DamageTargetsInRange(parent);
                    NotificationCenter.Notify(new Notification(NotificationType.GameState, 
                        NotificationAction.Health, [parent, -this.damage, true]));
                }
            }
        }
    }

    /**
     * Check to see if this unit is colliding with another unit that has already reach the
     * goToPos destination.
     */
    CheckSquadPos(sprite)
    {
        if(sprite.MoveBehavior.Finished)
        {
            let other = sprite.MoveBehavior.goToPos;
            if(other.X == this.goToPos.X && other.Y == this.goToPos.Y)
            {
                return true;
            }
        }
        return false;
    }

    ApplyInput(parent) {
        //if we have small left over values then zero
        if (Math.abs(parent.Body.velocityX) <= Body.MIN_SPEED)
          parent.Body.velocityX = 0;
        if (Math.abs(parent.Body.velocityY) <= Body.MIN_SPEED)
          parent.Body.velocityY = 0;
    
        //apply velocity to (x,y) of the parent's translation
        let translateBy = new Vector2(parent.Body.velocityX, parent.Body.velocityY)
        parent.Transform2D.TranslateBy(translateBy);
        parent.HealthBar.Transform2D.TranslateBy(translateBy);
        this.distance = this.CalculateDistance(parent.Transform2D.Translation, this.goToPos);
        if(this.distance < this.shortestDistance) {
            this.shortestDistance = this.distance;
        }
        else if(this.distance > this.shortestDistance + 50) {
            this.finished = true;
        }
    }

    DamageTargetsInRange(parent)
    {
        let enemyActors = this.team.enemyTargets;
        for(let i = 0; i < enemyActors.length; i++)
        {
            let targets = this.objectManager.Get(enemyActors[i]);
            for(let j = targets.length - 1; j >= 0; j--)
            {
                if(this.CalculateDistance(
                    parent.Transform2D.Translation, targets[j].Transform2D.Translation) <= this.splashRadius
                )
                {
                    NotificationCenter.Notify(new Notification(NotificationType.GameState, 
                        NotificationAction.Health, [targets[j], (-this.splashDamage)]));
                }
            }
        }
    }

    FindTarget(parent)
    {
        let enemyActors = this.team.enemyTargets;

        for(let i = 0; i < enemyActors.length; i++)
        {
            let targets = this.objectManager.Get(enemyActors[i]);
            for(let j = targets.length - 1; j >= 0; j--)
            {
                if(this.CalculateDistance(
                    parent.Transform2D.Translation, targets[j].Transform2D.Translation) <= this.range
                )
                {
                    parent.RightClick(targets[j].Transform2D.Translation, targets[j], false);
                    return;
                }
            }
        }
    }

    TargetCore(parent) {
        if(this.core != null) {
            if(this.target == null) {
                this.target = this.core;
                parent.RightClick(this.core.Transform2D.Translation, this.core, false);
            }
            if(this.target == this.core && !this.finished && this.timeSinceLastScan > SCAN_INTERVAL) {
                this.FindTarget(parent);
                this.timeSinceLastScan = 0;
            }
        }
    }

    Execute(gameTime, parent)
    {
        this.timeSinceLastScan += gameTime.ElapsedTimeInMs;
        if(this.finished && this.target == null && this.timeSinceLastScan > SCAN_INTERVAL)
        {
            this.FindTarget(parent);
            this.timeSinceLastScan = 0;
        }
        else this.TargetCore(parent);
        this.HandleMovement(gameTime, parent);
        this.ApplyForces(parent);
        this.HandleCollisions(parent);
        this.ApplyInput(parent);
    }

    RightClick(goToPos, parent, target = null)
    {
        this.CalculateRotation(parent, goToPos);
        if(target == null)
            this.goToPos = goToPos;
        else this.goToPos = target.Transform2D.Translation;

        this.shortestDistance = this.distance = this.CalculateDistance(
            parent.Transform2D.Translation, this.goToPos);
        this.finished = false;
        this.target = target;
    }

    CalculateRotation(parent, toPos)
    {
        let unitPos = parent.Transform2D.Translation;
        this.turnAngle = Math.atan2(toPos.Y - unitPos.Y, toPos.X - unitPos.X);
        this.turnAngle += Math.PI/180 * 90;

        this.turnAngle -= parent.Transform2D.RotationInRadians;
        if(this.turnAngle > Math.PI)
        {
            this.turnAngle -= (2*Math.PI);
        }
        else if(this.turnAngle < -Math.PI)
        {
            this.turnAngle += (2*Math.PI);
        }
    }

    GetMouseX(x)
    {
        return x - cvs.getBoundingClientRect().x;
    }
    GetMouseY(y)
    {
        return y - cvs.getBoundingClientRect().y;
    }

    //#endregion

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }


    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion

}