/**
 * Moves sprite based either on time-based or user input.
 * @author
 * @version 1.0
 * @class MoveBehavior
 */
class MoveBehavior
{
    constructor(moveDirection, moveSpeed)
    {
          this.moveDirection = moveDirection;
          this.moveSpeed = moveSpeed;
    }

    Execute(gameTime, parent)
    {
       let translateBy = Vector2.MultiplyScalar(this.moveDirection, gameTime.ElapsedTimeInMs * this.moveSpeed);
       parent.Transform2D.TranslateBy(translateBy);
    }

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

   Clone() {
    return new MoveBehavior(this.moveDirection, this.moveSpeed);
   }
    //#endregion

}