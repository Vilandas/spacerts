/**
 * Description goes here...
 * @author Vilandas Morrissey
 * @version 1.0
 * @class UnitShootBehavior
 */

class UnitShootBehavior {
    //#region  Fields 
    //#endregion 

    //#region  Properties
    //#endregion

    constructor(objectManager, notificationCenter, team, moveBehavior, objectData, bulletData)
    {
        this.id = "unitShootBehavior"; 
        this.objectManager = objectManager;
        this.notificationCenter = notificationCenter;
        this.team = team;
        this.moveBehavior = moveBehavior;
        this.bulletData = bulletData;

        this.range = objectData.shootProperties.projectileRange;
        this.damage = objectData.shootProperties.damage;
        this.splashDamage = objectData.shootProperties.splashDamage;
        this.splashRadius = objectData.shootProperties.splashRadius;
        this.fireInterval = objectData.shootProperties.shootIntervalInMS;
        this.projectileSpeed = objectData.shootProperties.projectileSpeed;
        this.timeSinceLastInMs = 0;
        this.projectiles = [];
        this.target = null;
        this.targetTranslation = null;
        this.userClick = false;
        this.RegisterForNotifications();
       // this.bulletOffset = Vector2.Subtract(objectData.takes[objectData.defaultTakeName].boundingBoxDimensions, bulletData.origin);
    }

    RegisterForNotifications() {
        this.notificationCenter.Register(
            NotificationType.Target,
            this,
            this.HandleNotification
        );
    }

    HandleNotification(...argArray)
    {
        let notification = argArray[0];
        switch (notification.NotificationAction) 
        {
            case NotificationAction.Destroy:
                this.HandleTargetDestroy(notification.NotificationArguments);
                break;
            default:
                break;
        }
    }

    HandleTargetDestroy(argArray)
    {
        if(this.target == argArray[0])
        {
            this.ResetTarget();
        }
    }

    ResetTarget()
    {
        this.target = null;
        this.moveBehavior.ResetTarget();
    }

    HandleShoot(gameTime, parent)
    {
        this.timeSinceLastInMs += gameTime.ElapsedTimeInMs;

        //Not yet in range
        if(this.moveBehavior.Distance > this.range)
        {
            //Moved out of range
            if(!this.userClick) {
                this.ResetTarget();
            }
            return;
        }

        if(this.timeSinceLastInMs >= this.fireInterval 
            && this.target != null 
            && this.moveBehavior.IsFinishedTurning()
        )
        {
            let projectile = new ProjectileObject(
                this.objectManager,
                this.bulletData,
                this.team,
                this.damage,
                this.target,
                parent.Transform2D.Translation.Clone(),
                parent.Transform2D.RotationInRadians,
                this.splashDamage,
                this.splashRadius
            );
            
            projectile.AttachBehavior(
                new MoveBehavior(parent.LookDirection, this.projectileSpeed)
            );

            this.projectiles.push(projectile);
            this.objectManager.Add(projectile);

            NotificationCenter.Notify(new Notification(
                NotificationType.Sound, NotificationAction.Play, ["laser"]));
            //reset
            this.timeSinceLastInMs = 0;
        }
    }

    HandleProjectileCollisions()
    {
        for(let i = this.projectiles.length - 1; i >= 0; i--)
        {
            if(this.projectiles[i].HandleCollisions())
            {
                this.objectManager.RemoveFirst(this.projectiles[i]);
                this.projectiles.splice(i, 1);
            }
        }
    }

    CheckTargetMoved(parent)
    {
        if(!this.targetTranslation.Equals(this.target.Transform2D.Translation))
        {
            //this.TargetReset();
            this.moveBehavior.CalculateRotation(parent, this.target.Transform2D.Translation);
        }
    }


    Execute(gameTime, parent)
    {
        this.HandleShoot(gameTime, parent);
        this.HandleProjectileCollisions();
        if(this.target != null)
        {
            this.CheckTargetMoved(parent);
        }
    }

    RightClick(target = null, userClick)
    {
        this.userClick = userClick;
        this.target = target;
        if(target != null)
            this.targetTranslation = target.Transform2D.Translation.Clone();
    }

    //#endregion

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }


    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}