/**
 * 
 * @author
 * @version 1.0
 * @class BuildUnitBehavior
 */
class BuildUnitBehavior {
  //#region Static Fields
  //#endregion

  //#region Fields
  //#endregion

  //#region Properties
  //#endregion

  constructor(
    keyboardManager,
    objectManager,
    notificationCenter,
    team,
    objectData,
    startPosition
  ) {
    this.keyboardManager = keyboardManager;
    this.objectManager = objectManager;
    this.notificationCenter = notificationCenter;
    this.team = team;
    this.diameter = objectData.collisionProperties.circleRadius*2;

    this.production = PRODUCTION_DATA;
    this.timer = new Stopwatch();
    this.productionBar = new HealthBar(objectManager, Vector2.Add(startPosition, new Vector2(0, HEALTHBAR_HEIGHT)),
      this.diameter, Color.CornFlowerBlue);
    this.productionBar.SetBarByFraction(0);
    this.objectToProduce = null;
    this.validPurchase = false;
    this.RegisterForNotifications();
  }

  RegisterForNotifications() {
    this.notificationCenter.Register(
      NotificationType.Currency,
      this,
      this.HandleNotification
    );
  }

  HandleNotification(...argArray) {
    let notification = argArray[0];
    switch (notification.NotificationAction) {
      case NotificationAction.Get:
        this.validPurchase = notification.NotificationArguments[0];
        break;
    }
  }

  HandleInput(gameTime, parent) {
    if(!this.timer.IsRunning) {
      for(let item in this.production)
      {
        if(this.keyboardManager.IsKeyDown(this.production[item].key) && this.CheckIncome(this.production[item].cost)) {
          this.objectToProduce = this.production[item];
          this.timer.Start();
        }
      }
    }
    else {
      if(this.timer.GetElapsedTime() >= this.objectToProduce.buildTime) {
        switch(this.objectToProduce) {
          case this.production.drone:
            this.objectManager.Add(new F1Drone(this.objectManager, this.team,
              Vector2.Add(parent.Transform2D.Translation, new Vector2(0, parent.Transform2D.Dimensions.Y))));
            break;

          case this.production.fighter:
            this.objectManager.Add(new F1Fighter(this.objectManager, this.notificationCenter, this.team,
              Vector2.Add(parent.Transform2D.Translation, new Vector2(0, parent.Transform2D.Dimensions.Y))));
            break;

          case this.production.buster:
            this.objectManager.Add(new F1Buster_Drone(this.objectManager, this.team,
            Vector2.Add(parent.Transform2D.Translation, new Vector2(0, parent.Transform2D.Dimensions.Y))));
          break;

          case this.production.plasma:
            this.objectManager.Add(new F1Plasma_Ship(this.objectManager, this.notificationCenter, this.team,
              Vector2.Add(parent.Transform2D.Translation, new Vector2(0, parent.Transform2D.Dimensions.Y))));
            break;
        }
        this.timer.Reset();
        this.productionBar.SetBarByFraction(0);
        this.objectToProduce = null;
      }
    }
  }

  CheckIncome(cost) {
    NotificationCenter.Notify(new Notification(NotificationType.GameState, 
      NotificationAction.Get, [cost]));
    return this.validPurchase;
  }

  UpdateProgressBar(parent) {
    if(this.objectToProduce != null) {
      this.productionBar.SetBarByFraction(this.timer.GetElapsedTime()/this.objectToProduce.buildTime);
    }
    this.productionBar.Transform2D.Translation = Vector2.Add(parent.Transform2D.Translation,
      new Vector2(0, HEALTHBAR_HEIGHT + this.diameter));
  }

  Execute(gameTime, parent) {
    this.HandleInput(gameTime, parent);
    this.UpdateProgressBar(parent);
  }

  //#region Common Methods - Equals, ToString, Clone
  Equals(other) {
    //to do...
    throw "Not Yet Implemented";
  }

  ToString() {
    //to do...
    throw "Not Yet Implemented";
  }

  Clone() {
    //to do...
    throw "Not Yet Implemented";
  }
  //#endregion
}