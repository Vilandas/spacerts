/**
 * Moves the parent sprite based on keyboard input and detect collisions against platforms, pickups etc
 * @author
 * @version 1.0
 * @class PlayerBehavior
 */
class CoopShipBehavior {
  //#region Static Fields
  //#endregion

  //#region Fields
  //#endregion

  //#region Properties
  //#endregion

  constructor(
    keyboardManager,
    objectManager,
    camera = null,
    team,
    objectData,
    bulletData
  ) {
    this.keyboardManager = keyboardManager;
    this.objectManager = objectManager;
    this.camera = camera;
    this.team = team;
    this.bulletData = bulletData;

    this.moveKeys = objectData.moveProperties.moveKeys;
    this.moveSpeed = objectData.moveProperties.moveSpeed;
    this.rotateSpeed = objectData.moveProperties.rotateSpeedInRadians;
    this.originalDirection = objectData.moveProperties.lookDirection.Clone(); //direction we were facing at game start
    this.lookDirection = objectData.moveProperties.lookDirection.Clone(); //direction after we turn

    this.shootProperties = objectData.shootProperties;
    this.projectiles = [];
    this.timeSinceLastInMs = 0;
  }

  SetCamera(camera, parent) {
    this.camera = camera;
    this.MoveCamera(parent.Transform2D.Translation);
  }

  MoveCamera(parentTranslation) {
    this.camera.Transform2D.Translation = Vector2.Subtract(
      parentTranslation, Vector2.Divide(
        this.camera.Transform2D.Dimensions, new Vector2(2, 2)));
  }

  UpdateLookDirection(parent) {
    parent.LookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians)));
  }

  //#region Your Game Specific Methods - add code for more CD/CR or input handling
  HandleInput(gameTime, parent) {
    this.HandleMove(gameTime, parent);
    this.HandleShoot(gameTime, parent);
    this.HandleProjectileCollisions();
  }

  HandleShoot(gameTime, parent)
  {
    this.timeSinceLastInMs += gameTime.ElapsedTimeInMs;

    if (this.keyboardManager.IsKeyDown(this.shootProperties.shootKeys.shoot)
      && this.timeSinceLastInMs >= this.shootProperties.shootIntervalInMS)
    {
      this.UpdateLookDirection(parent);
      let originalOffset = Vector2.Transform(this.shootProperties.projectileOffset,
        Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians));
      let offset = Vector2.MultiplyScalar(originalOffset, -this.shootProperties.projectileCount/2);
      for(let i = 0; i < this.shootProperties.projectileCount; i++)
      {
        if(i == this.shootProperties.projectileCount/2)
          offset = Vector2.Add(offset, originalOffset);        
        let projectile = new ProjectileObject(
          this.objectManager,
          this.bulletData,
          this.team,
          this.shootProperties.damage,
          null,
          Vector2.Add(parent.Transform2D.Translation.Clone(), offset),
          parent.Transform2D.RotationInRadians
        );

        offset = Vector2.Add(offset, originalOffset);
      
        projectile.AttachBehavior(
            new MoveBehavior(parent.LookDirection, this.shootProperties.projectileSpeed)
        );

        NotificationCenter.Notify(new Notification(
          NotificationType.Sound, NotificationAction.Play, ["laser"]));

        this.projectiles.push(projectile);
        this.objectManager.Add(projectile);
      }
      //reset
      this.timeSinceLastInMs = 0;
    }
  }

  HandleProjectileCollisions()
  {
      for(let i = this.projectiles.length - 1; i >= 0; i--)
      {
          if(this.projectiles[i].HandleCollisions())
          {
              this.objectManager.RemoveFirst(this.projectiles[i]);
              this.projectiles.splice(i, 1);
          }
      }
  }

  HandleMove(gameTime, parent) {
    //Player idle
    if(this.keyboardManager.AreKeysDown([this.moveKeys.up, this.moveKeys.left, this.moveKeys.down, this.moveKeys.right]) == false)
    {
      parent.Artist.SetTake("idle");
    }

    //Move forward
    if (this.keyboardManager.IsKeyDown(this.moveKeys.up)) {
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians)));
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, gameTime.ElapsedTimeInMs * this.moveSpeed));
      parent.Artist.SetTake("idle");
    }
    //Move back
    else if (this.keyboardManager.IsKeyDown(this.moveKeys.down)) {
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians)));
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, gameTime.ElapsedTimeInMs * -this.moveSpeed));
      parent.Artist.SetTake("idle");
    }
    //Rotate left
    if (this.keyboardManager.IsKeyDown(this.moveKeys.left)) {
      parent.Transform2D.RotateBy(-this.rotateSpeed);
      parent.Artist.SetTake("idle");
    }
    //Rotate Right
    else if (this.keyboardManager.IsKeyDown(this.moveKeys.right)) {
      parent.Transform2D.RotateBy(this.rotateSpeed);
      parent.Artist.SetTake("idle");
    }
  }

  HandleCollisions(parent)
  {
    for(let i = 0; i < COLLIDABLE_MOVE_ACTORS.length; i++)
    {
      let sprites = this.objectManager.Get(COLLIDABLE_MOVE_ACTORS[i]);
      this.CheckCollisions(parent, sprites);
    }
  }

  CheckCollisions(parent, sprites) 
  {
    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];
      if(sprite.CollisionType) {
        let collisionLocationType = Collision.GetIntersectsLocation(parent, sprite);

        //the code below fixes a bug which caused sprites to stick inside an object
        if (collisionLocationType === CollisionLocationType.Left) {
          if (parent.Body.velocityX <= 0) {
            parent.Body.SetVelocityX(0);
          }
        }
        else if (collisionLocationType === CollisionLocationType.Right) {
          if (parent.Body.velocityX >= 0) {
            parent.Body.SetVelocityX(0);
          }
        }
        //the code below fixes a bug which caused sprites to stick inside an object
        if (collisionLocationType === CollisionLocationType.Top) {
          if (parent.Body.velocityY <= 0) {
            parent.Body.SetVelocityY(0);
          }
        }
        else if (collisionLocationType === CollisionLocationType.Bottom) {
          if (parent.Body.velocityY >= 0) {
            parent.Body.SetVelocityY(0);
          }
        }
      }
    }
  }


  ApplyForces(parent) {
    //notice we need to slow body in X and Y and we dont ApplyGravity() in a top-down game
    parent.Body.ApplyFrictionX();
    parent.Body.ApplyFrictionY();
  }

  ApplyInput(parent) {
    //if we have small left over values then zero
    if (Math.abs(parent.Body.velocityX) <= Body.MIN_SPEED)
      parent.Body.velocityX = 0;
    if (Math.abs(parent.Body.velocityY) <= Body.MIN_SPEED)
      parent.Body.velocityY = 0;

    //apply velocity to (x,y) of the parent's translation
    let translateBy = new Vector2(parent.Body.velocityX, parent.Body.velocityY)
    parent.Transform2D.TranslateBy(translateBy);
    parent.HealthBar.Transform2D.TranslateBy(translateBy);
    if(this.camera != null) {
      translateBy = this.CheckOutOfBounds(parent);
      this.MoveCamera(translateBy);
    }
  }

  CheckOutOfBounds(parent) {
    let newLocation = parent.Transform2D.Translation.Clone();
    if(newLocation.X < 325)
      newLocation.X = 325;
    else if(newLocation.X > 2750)
      newLocation.X = 2750;

    if(newLocation.Y < 400)
      newLocation.Y = 400;
    else if(newLocation.Y > 1708)
      newLocation.Y = 1708;
    return newLocation;
  }

  //#endregion

  Execute(gameTime, parent) {
    this.HandleInput(gameTime, parent);
    this.ApplyForces(parent);
    this.HandleCollisions(parent);
    this.ApplyInput(parent);
  }

  //#region Common Methods - Equals, ToString, Clone
  Equals(other) {
    //to do...
    throw "Not Yet Implemented";
  }

  ToString() {
    //to do...
    throw "Not Yet Implemented";
  }

  Clone() {
    //to do...
    throw "Not Yet Implemented";
  }
  //#endregion
}