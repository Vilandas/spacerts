//#region Development Diary
/*

Week 7 
------
Comments: 
- GDUtility.IsSameTypeAsTarget() and its impact on Equals() methods in classes.

Exercises: 
- Re-factor CameraManager to align with ObjectManager use of predicates etc.
- Use ContextParameter classes in the appropriate artist.

Notes:
- None

To Do:
- Wrap MyConstants.js and Constants.js in a class to set scope and no longer pollute global project space.
- Add booleans to DebugDrawer to enable/disable drawing of BBs for objects and camera, and drawing of debug text.
- Improve SoundManager to block load until all sound resources have loaded.
- Add pause/unpause to SoundManager when we lose/gain window focus.
- Add code to calculate TextSpriteArtist bounding box size based on text used.
- Fix background UpdateHorizontalScrolling().
- Add countdown toast when we gain window focus.
- Add check for "P" key in MyMenuManager::Update() to show/hide menu
- Improve KeyboardManager to add IsFirstKeyPress() method.
- Complete menu demo.
- Continue adding documentation to all classes and methods.

Done:
- Add context to the camera class.
- Complete the RenderManager to move ObjectManager::Draw()
- When we scroll L/R and the active camera NO LONGER intersects the centre background image (i.e. its bounding box) then the background is no drawn - See ObjectManager::Draw() specifically (if(sprite.ActorType == ActorType.Background ||...) 
- Added GDUtility.IsSameTypeAsTarget() 
- Added Matrix class in preparation for rotating sprites.

Bugs:
- Camera bounding box is not updating on camera scale, rotate.
- When we scroll too far L/R then scrolling stops - see ScrollingSpriteArtist.
- When we use background scroll <- and -> then collisions are not tested and responded to
- When player and platform above are separated by only player height?


Week 6 
------
Notes:
- None

To Do:
- Add booleans to DebugDrawer to enable/disable drawing of BBs for objects and camera, and drawing of debug text.
- Re-factor CameraManager to align with ObjectManager use of predicates etc?
- Improve SoundManager to block load until all sound resources have loaded.
- Add pause/unpause to SoundManager when we lose/gain window focus.
- Add code to calculate TextSpriteArtist bounding box size based on text used.
- Fix background UpdateHorizontalScrolling().
- Add countdown toast when we gain window focus.
- Add check for "P" key in MyMenuManager::Update() to show/hide menu
- Improve KeyboardManager to add IsFirstKeyPress() method.
- Complete menu demo.
- Continue adding documentation to all classes and methods.

Done:
- Removed unused GDArray.
- Added "if (this == other) return true;" to Equals() methods to test if this and other are same object.
- Add Vector2::Distance().
- Fix bounding box on TextSpriteArtist - see constants::TextBaselineType and https://www.w3schools.com/tags/canvas_textbaseline.asp and https://www.w3schools.com/tags/canvas_measuretext.asp
- Re-introduce culling in MyObjectManager::Draw() based on Camera2D bounding box.
- Add DebugDrawer::Draw() and show object count, fps etc.
- Removed ObjectManager::DrawDebugBoundingBox and created DebugDrawer to handle showing debug (bounding boxes, FPS) related information
- Added CollisionType to allow us to say which actors are collidable, or not.
- Re-factor MyObjectManager.
- Added predicate based remove, find, and sort functions to CameraManager.
- Fix no translate on numpad keys left, right.
- Add Camera2D and re-factor use of translationOffset in Transform2D and artist Draw().
- Add CameraManager to update all cameras.
- Complete TextSpriteArtist.
- Improve AnimatedSpriteArtist to store all animations for a sprite inside an object and not in a single array of cells.

Bugs:
- Camera2D bounding box is not updating with scale changes.
- Camera bounding box is not updating on camera scale, rotate.
- When we scroll L/R and the active camera NO LONGER intersects the centre background image (i.e. its bounding box) then the background is no drawn.
- When we scroll too far L/R then scrolling stops - see ScrollingSpriteArtist.
- When we use background scroll <- and -> then collisions are not tested and responded to
- When player and platform above are separated by only player height?

Week 5 
------
Notes:
- None

To Do:
- Continue adding documentation to all classes and methods.
- Complete TextSpriteArtist.
- Improve AnimatedSpriteArtist to store all animations for a sprite inside an object and not in a single array of cells.
- Improve SoundManager to block load until all sound resources have loaded.
- Improve KeyboardManager to add IsFirstKeyPress() method.
- Add Camera2D and re-factor use of translationOffset in Transform2D and artist Draw().
- Complete menu demo.

Done:

Bugs:
- When we scroll too far L/R then scrolling stops - see ScrollingSpriteArtist.
- When we use background scroll <- and -> then collisions are not tested and responded to
- When player and platform above are separated by only player height?

Week 4 
------
Notes:
- Runner jump and run values are finely balanced with friction and gravity - tweak these values for your own game - see Body

To Do:
- Complete menu demo.

Done:
- Added MyObjectManager pause event handling.
- Commented in code to play jump and background music (when you collide with the wasp/insect from the top)
- Fixed bug in AnimatedSpriteArtist Cells getter which would not reset animation frames if animation changed (e.g. from walk left to walk right).
- In MyObjectManager::RegisterForNotifications() removed reference to HandleSpriteNotification.
- In MyMenuManager::Initialize() changed this.notificationCenter.Notify() to notificationCenter.Notify().

Bugs:
- When we use background scroll <- and -> then collisions are not tested and responded to
- When jumping and touch platform to L or R - allows double jump?
- When player and platform above are separated by only player height?

Week 3 
------
Notes:
- Runner jump and run values are finely balanced with friction and gravity - tweak these values for your own game - see Body

To Do:
- Use 

Done:
- Added NotificationCenter instanciation in game.js to support sound, sprite removal, and game state events (e.g. inventory, ammo, pickup, health)
- Added HandleNotification() methods to MyObjectManager, SoundManager, and MyGameStateManager to listen for relevant NotificationCenter events - see PlayerBehavior::HandlePickupCollision()
- Added new AudioType to support damage and pickup category audio
- Added new ActorTypes to support ammo, pickup, inventory
- Simplified CD/CR in PlayerBehavior to check for platform, enemy, pickup collisions in separate methods
- Added Body class to support friction, velocity, and gravity
- Added MoveableSprite for sprites that will have an associated Body object
- Re-factored jump/fall physics in PlayerBehavior to reduce CPU overhead and add gravity and friction
- Added new objects in MyConstants to represent BACKGROUND_DATA and PLATFORM_DATA
- Added draw culling (dont draw sprites outside screen) to MyObjectManager::DrawAll()
- Added ScrollingBackgroundArtist and renamed to ScrollingSpriteArtist
- Added RectangleSpriteArtist
- Added initial fall code

Bugs:
- When jumping and touch platform to L or R - allows double jump?
- When player and platform above are separated by only player height?

Week 2 
------
To Do:
- Add PlayerMoveBehavior::ExecuteFall()
- Add ScrollingBackgroundArtist
- Add PlatformArtist

Done:
- Added RectangleSpriteArtist
- Added initial fall code

Week 1
------
To Do:
- Add PlayerMoveBehavior
- Add ScrollingBackgroundArtist
- Add PlatformArtist

Done:

*/

//#endregion

class Game {

  /************************************************************ CORE CODE THAT DOESN'T CHANGE EXCEPT WHEN ADDING/REMOVING/REFACTORING MANAGERS ************************************************************/

  //#region Fields
  //canvas and context
  screens = [];

  //game resources
  spriteSheet;
  jungleSpriteSheet;

  //time object and notification 
  gameTime;
  notificationCenter;

  //managers
  objectManager;
  soundManager;
  gameStateManager;
  cameraManager;

  //other
  selectionBool;
  selectionPoint;
  capitalShip;
  isPaused;

  //debug
  debugModeOn;
  //#endregion

  //#region Constructor
  constructor(debugModeOn) {
    //enable/disable debug info
    this.debugModeOn = debugModeOn;
  }
  //#endregion

  // #region LoadGame, Start, Animate
  LoadGame() {

    //load content
    this.Initialize();

    //publish an event to pause the object manager (i.e. no update) and render manager (i.e. no draw) and show the menu
    NotificationCenter.Notify(
      new Notification(
        NotificationType.Menu,
        NotificationAction.ShowMenuChanged,
        [StatusType.Off, "canvas1"]
      )
    );

    //start timer - notice that it is called only after we loaded all the game content
    this.Start();
  }

  Start() {
    //runs in proportion to refresh rate
    this.gameTime = new GameTime();
    this.animationTimer = window.requestAnimationFrame(this.Animate.bind(this));
  }

  Animate(now) {  
    this.gameTime.Update(now);
    this.Update(this.gameTime);
    this.Draw(this.gameTime);
    window.requestAnimationFrame(this.Animate.bind(this));
  }
  // #endregion

  // #region Update, Draw
  Update(gameTime) {
    if(this.isPaused) {
      NotificationCenter.Notify(new Notification(
        NotificationType.Sound, NotificationAction.Play, ["menu_background"]));
    }
    else if (this.keyboardManager.IsKeyDown(Keys.P) && !this.isPaused)
    {
      this.isPaused = true;
      if(this.screens.length == 2)
        NotificationCenter.Notify(new Notification(NotificationType.Menu,
          NotificationAction.ShowMenuChanged, [StatusType.Off | StatusType.IsDrawn, this.screens[0].canvasID, this.screens[1].canvasID]));
      else
        NotificationCenter.Notify(new Notification(NotificationType.Menu,
          NotificationAction.ShowMenuChanged, [StatusType.Off | StatusType.IsDrawn, this.screens[0].canvasID]));
    }
    else if(!this.isPaused) {
      //update all the game sprites
      this.objectManager.Update(this.gameTime);

      // //update game state
      this.gameStateManager.Update(gameTime);

      // //updates the menu manager to listen for show/hide menu keystroke
      // this.menuManager.Update(gameTime);

      //updates the camera manager which in turn updates all cameras
      this.cameraManager.Update(gameTime);

      //DEBUG - REMOVE LATER
      if (this.debugModeOn)
        this.debugDrawer.Update(gameTime);
    }
  }

  Draw(gameTime) {
    //clear screen on each draw of ALL sprites (i.e. menu and game sprites)
    for(let i = 0; i < this.screens.length; i++)
      this.ClearScreen(Color.Black, this.screens[i].cvs, this.screens[i].ctx);

    //draw all the game sprites
    this.renderManager.Draw(gameTime);
    this.gameStateManager.Draw(gameTime);

    //DEBUG - REMOVE LATER
    if (this.debugModeOn)
      this.debugDrawer.Draw(gameTime);
  }

  ClearScreen(color, cvs, ctx) {
    ctx.save();
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, cvs.clientWidth, cvs.clientHeight);
    ctx.restore();
  }
  // #endregion

  /************************************************************ YOUR GAME SPECIFIC UNDER THIS POINT ************************************************************/
  // #region Initialize, Load - Debug, Cameras, Managers
  Initialize() {
    this.LoadCanvases();
    this.LoadAssets();
    this.LoadNotificationCenter();
    this.LoadInputAndCameraManagers();
    this.LoadCameras(); //make at the end as 1+ behaviors in camera may depend on sprite
    this.LoadAllOtherManagers();
    this.RegisterForNotifications();
    this.LoadSprites();
    this.LoadEventListeners();

    // //set game is playing
    // this.isPlaying = false;

    //DEBUG - REMOVE LATER
    if (this.debugModeOn)
      this.LoadDebug();

  }

  LoadCanvases() {
    //get a handle to our context
    //playerID, cameraID, parentDivID, canvasID, introID, uiID, topLeft, dimensions, clearScreenColor
    this.screens.push(GDGraphics.GetScreenObject("player 1", "camera p1", "parent-top", "canvas1", "player-intro-top", "player-ui-top",
    new Vector2(0,0), new Vector2(1600, 800), Color.LightGreen));
  }

  LoadCoopPlayer()
  {
    this.screens.push(GDGraphics.GetScreenObject("player 2", "camera p2", "parent-bottom", "canvas2", "player-intro-bottom", "player-ui-bottom",
    new Vector2(0,0), new Vector2(650, 800), Color.DarkGreen));

    let transform = new Transform2D(
      new Vector2(0, 0),
      0,
      new Vector2(1, 1),
      new Vector2(0, 0),
      new Vector2(this.screens[1].dimensions.x, this.screens[1].dimensions.y)
    );

    let camera2 = new Camera2D(
      "camera p2",
      ActorType.Camera,
      transform,
      StatusType.IsUpdated,
      this.screens[1].ctx
    );

    camera2.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

    this.cameraManager.Add(camera2);

    this.capitalShip.SetCamera(camera2);

    this.gameStateManager.InitializeUI(this.screens[1].uiID);
  }

  GetTargetPlayer(playerIndex) {
    if (playerIndex >= 0 && playerIndex < this.playerSprites.length)
      return this.playerSprites[playerIndex];
    else
      throw "Error: A behavior (e.g. TrackTargetTranslationBehavior) is looking for a player index that does not exist. Are there sufficient sprites in the playerSprites array?";
  }

  LoadCameras() {
    //#region Camera 1  
    let transform = new Transform2D(
      new Vector2(0, 0),
      0,
      new Vector2(1, 1),
      new Vector2(0, 0),
      new Vector2(this.screens[0].dimensions.x, this.screens[0].dimensions.y)
    );

    let camera = new Camera2D(
      "camera p1",
      ActorType.Camera,
      transform,
      StatusType.IsUpdated,
      this.screens[0].ctx
    );

    /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/
    camera.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

    /**************** NEED TO ADD A BEHAVIOR TO MAKE THIS A CONTROLLABLE ACTOR ***********/
    //cameraTop.AttachBehavior(new TrackTargetTranslationBehavior(this, 0, new Vector2(0, 120)));

    camera.AttachBehavior(
      new FlightCameraBehavior(
        this.keyboardManager,
        CAMERA_KEYS,
        new Vector2(10, 0),
        Math.PI / 180,
        new Vector2(0.005, 0.005),
        this.screens[0].cvs,
        camera
      )
    );

    camera.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);

    this.cameraManager.Add(camera);
    //#endregion
  }

  LoadNotificationCenter() {
    this.notificationCenter = new NotificationCenter();
  }

  
  RegisterForNotifications() {
    //register for menu notification
    this.notificationCenter.Register(
        NotificationType.Main,
        this,
        this.HandleNotification
    );
  }

  HandleNotification(...argArray) {
    let notification = argArray[0];
    switch (notification.NotificationAction) {
      case NotificationAction.Add:
        this.PickCapital();
        this.LoadCoopPlayer();
        break;
      case NotificationAction.Resume:
        this.isPaused = notification.NotificationArguments[0];
        break;
      case NotificationAction.Win:
        window.location.reload();
        break;
      case NotificationAction.SpawnCapital:
        this.PickCapital();
        break;
    }
  }

  LoadInputAndCameraManagers() {
    //checks for keyboard input
    this.keyboardManager = new KeyboardManager();
    //stores the cameras in our game
    this.cameraManager = new CameraManager("stores and manages cameras");
  }

  LoadAllOtherManagers() {
    this.isPaused = true;
    //update objects
    this.objectManager = new ObjectManager(
      "game sprites",
      StatusType.IsUpdated,
      this.cameraManager,
      this.notificationCenter
    );

    //draw objects
    this.renderManager = new RenderManager(
      "draws sprites in obj manager",
      StatusType.IsDrawn,
      this.objectManager,
      this.cameraManager,
      this.notificationCenter);

    //audio - step 3 - instanciate the sound manager with the array of cues
    this.soundManager = new SoundManager(
      audioCueArray,
      this.notificationCenter);

    //add the manager that listens for events (win, lose, respawn) within the game and reacts
    this.gameStateManager = new MyGameStateManager(
      "listens for game state changes",
      StatusType.IsUpdated,
      this.objectManager, this.cameraManager,
      this.notificationCenter,
      this.screens[0].ctx,
      this.screens
    );

    //adds support for a menu system
    this.menuManager = new MyMenuManager(
      this.screens[0].canvasID, this.notificationCenter, this.keyboardManager,
      this.screens[0].parentDivID, this.screens
    );
  }

  LoadDebug() {
    this.debugDrawer = new DebugDrawer("shows debug info", StatusType.IsDrawn,
      this.objectManager, this.cameraManager,
      this.notificationCenter,
      DebugDrawType.ShowDebugText | DebugDrawType.ShowSpriteCollisionPrimitive);
  }
  //#endregion

  //#region Load(Assets, Sprites)
  LoadAssets() {
    //what could we use this for?
  }
  LoadSprites() {
    let objectData = BACKGROUND_DATA;
    let artist = new SpriteArtist(
      objectData.spriteSheet, 
      objectData.sourcePosition, 
      objectData.sourceDimensions);
    
    let transform = new Transform2D(
      objectData.sourcePosition,
      objectData.rotationInRadians,
      objectData.scale,
      objectData.origin,
      objectData.sourceDimensions
    );
    
    let backgroundSprite = new Sprite(
        objectData.id, 
        objectData.actorType, 
        objectData.collisionProperties.type,
        transform, artist,
        objectData.statusType,
        objectData.scrollSpeedMultiplier,
        objectData.layerDepth,
    );

    backgroundSprite.CollisionPrimitive = new RectCollisionPrimitive(transform, 0);
    this.objectManager.Add(backgroundSprite);
    //load the level walls etc
    this.LoadMultipleSpritesFrom2DArray(LEVEL_ARCHITECTURE_DATA, new Vector2(0,0));
    this.LoadStartingShips();
  }

  LoadStartingShips()
  {
    this.objectManager.Add(new Selection());

    this.objectManager.Add(new F1Fighter(this.objectManager, this.notificationCenter, TEAM.t1, new Vector2(1200, 1200)));
  }

  PickCapital() {
    let capital = document.querySelector("#capital").value;
    if(capital == "tank")
      this.capitalShip = new F1Capital_Tank(this.objectManager, this.keyboardManager, this.notificationCenter, null, TEAM.t1, new Vector2(1000, 1000));
    if(capital == "goliath")
      this.capitalShip = new F1Capital_Goliath(this.objectManager, this.keyboardManager, this.notificationCenter, null, TEAM.t1, new Vector2(1000, 1000));

  }

  LoadMultipleSpritesFrom2DArray(theObject, translationOffset) {
    let maxRows = theObject.levelLayoutArray.length;
    let maxCols = theObject.levelLayoutArray[0].length;
    let blockWidth = theObject.maxBlockWidth;
    let blockHeight = theObject.maxBlockHeight;
    let transform = null;
    let artist = null;
    let sprite = null;

    for (let row = 0; row < maxRows; row++) {
      for (let col = 0; col < maxCols; col++) {
        //we read a number from the array (and subtract 1 because 0 is our draw nothing value)
        let levelSpritesNumber = theObject.levelLayoutArray[row][col];

        //if we get a value of 0 from the  we have nothing to draw
        if (levelSpritesNumber != 0) {
          transform = new Transform2D(
            Vector2.Add(new Vector2(col * blockWidth, row * blockHeight), translationOffset),
            theObject.levelSprites[levelSpritesNumber].rotation,
            theObject.levelSprites[levelSpritesNumber].scale,
            theObject.levelSprites[levelSpritesNumber].origin,
            theObject.levelSprites[levelSpritesNumber].sourceDimensions);

          //remember we can also add an animated artist instead
          artist = new SpriteArtist(theObject.levelSprites[levelSpritesNumber].spriteSheet,
            theObject.levelSprites[levelSpritesNumber].sourcePosition,
            theObject.levelSprites[levelSpritesNumber].sourceDimensions,
            theObject.levelSprites[levelSpritesNumber].alpha);

          sprite = new Sprite("block[" + row + "," + col + "]",
            theObject.levelSprites[levelSpritesNumber].actorType,
            theObject.levelSprites[levelSpritesNumber].collisionProperties.type,
            transform, artist,
            theObject.levelSprites[levelSpritesNumber].statusType,
            theObject.levelSprites[levelSpritesNumber].scrollSpeedMultiplier,
            theObject.levelSprites[levelSpritesNumber].layerDepth);

          /**************** NEED TO ATTACH A COLLISION PRIMITIVE (e.g. CIRCLE OR RECTANGLE) ****************/

           if(theObject.levelSprites[levelSpritesNumber].collisionProperties.primitive == CollisionPrimitiveType.Circle)
           {
             sprite.CollisionPrimitive = new CircleCollisionPrimitive(transform, theObject.levelSprites[levelSpritesNumber].collisionProperties.circleRadius);
           }
           else
           {
             sprite.CollisionPrimitive = new RectCollisionPrimitive(transform, theObject.levelSprites[levelSpritesNumber].collisionProperties.explodeRectangleBy);    
           }

          //do we want to add behaviors if so then add them here?

          this.objectManager.Add(sprite);
        }
      }
    }
  }
  
  //#endregion
  LoadEventListeners()
  {
    this.screens[0].cvs.addEventListener("mousedown", (e) => this.StartSelection(e, this));
    this.screens[0].cvs.addEventListener("mouseup", (e) => this.EndSelection(e, this));
    this.screens[0].cvs.addEventListener("mousemove", (e) => this.DragSelection(e, this));
    this.screens[0].cvs.addEventListener("contextmenu", (e) => this.RightClick(e, this));
  }
  
  RightClick(e, game)
  {
    e.preventDefault();
    game.gameStateManager.RightClick(game.GetMouseCoords(e.clientX, e.clientY));
  }

  //#region Mouse controls
  StartSelection(e, game) 
  {
    //left click only
    if(e.button == 0)
    {
      //clear allready selected units
      NotificationCenter.Notify(new Notification(
        NotificationType.GameState, NotificationAction.ClearSelection
      ));

      let hudSprites = game.objectManager.Get(ActorType.HUD);
      for(let i = 0; i < hudSprites.length; i++)
      {
        if(hudSprites[i].ID == "selection")
        {
          game.selectionBool = true;
          let mouse = game.GetMouseCoords(e.clientX, e.clientY);
          hudSprites[i].Transform2D.SetTranslationX(mouse.X);
          hudSprites[i].Transform2D.SetTranslationY(mouse.Y);
          game.CheckSelectedUnits(hudSprites[i]);
          break;
        }
      }
    }
  }
  DragSelection(e, game)
  {
    if(e.button == 0)
    {
      if(game.selectionBool)
      {
        let hudSprites = game.objectManager.Get(ActorType.HUD);
        for(let i = 0; i < hudSprites.length; i++)
        {
          let sprite = hudSprites[i];
          if(sprite.ID == "selection")
          {
            let mouse = game.GetMouseCoords(e.clientX, e.clientY);
            sprite.Transform2D.Dimensions = new Vector2(
              mouse.X - sprite.Transform2D.Translation.X,
              mouse.Y - sprite.Transform2D.Translation.Y
            );  
            game.CheckSelectedUnits(hudSprites[i]);
          }
        }
      }
    }
  }
  EndSelection(e, game)
  {
    if(e.button == 0)
    {
      let sprites = game.objectManager.Get(ActorType.HUD);
      for(let i = 0; i < sprites.length; i++)
      {
        if(sprites[i].ID == "selection")
        {
          game.selectionBool = false;
          game.dragLock = false;
          sprites[i].Transform2D.SetTranslationX(-1);
          sprites[i].Transform2D.SetTranslationY(-1);
          sprites[i].Transform2D.Dimensions = new Vector2(0, 0);
        }
      }
    }
  }

  GetMouseCoords(x, y)
  {
    //console.log(this.cameraManager.ActiveCamera.Transform2D);
    return new Vector2(
      x - this.screens[0].cvs.getBoundingClientRect().x
      + this.cameraManager.ActiveCamera.Transform2D.Translation.X,
      //-(this.screens[0].cv),
      y - this.screens[0].cvs.getBoundingClientRect().y + this.cameraManager.ActiveCamera.Transform2D.Translation.Y
    );
  }

  CheckSelectedUnits(selectionBox)
  {
    let normalisedSelectionBox = selectionBox.Clone();
    //normalisedSelectionBox = new Selection(this.objectManager);
    if(selectionBox.Transform2D.Dimensions.X < 0)
    {
      normalisedSelectionBox.Transform2D.SetTranslationX(
        selectionBox.Transform2D.Translation.X + selectionBox.Transform2D.Dimensions.X);
      normalisedSelectionBox.Transform2D.Dimensions.X = Math.abs(selectionBox.Transform2D.Dimensions.X);
    }
    if(selectionBox.Transform2D.Dimensions.Y < 0)
    {
      normalisedSelectionBox.Transform2D.SetTranslationY(
        selectionBox.Transform2D.Translation.Y + selectionBox.Transform2D.Dimensions.Y);
      normalisedSelectionBox.Transform2D.Dimensions.Y = Math.abs(selectionBox.Transform2D.Dimensions.Y);
    }

    let sprites = this.objectManager.Get(ActorType.Unit);
    for(let i = 0; i < sprites.length; i++)
    {
      if(Collision.Intersects(normalisedSelectionBox, sprites[i]))
      {
        NotificationCenter.Notify(new Notification(
          NotificationType.GameState, NotificationAction.AddOneSelection,
          [sprites[i]]));
      }
      else
      {
        NotificationCenter.Notify(new Notification(
          NotificationType.GameState, NotificationAction.RemoveOneSelection,
          [sprites[i]]));
      }
    }

  }

  //#endregion
}

//instead of "load" we could use "DOMContentLoaded" but this would indicate load complete when the HTML and DOM is loaded and NOT when all styling, scripts and images have been downloaded
window.addEventListener("load", event => {
  let bDebugMode = true;
  let game = new Game(bDebugMode);
  game.LoadGame();
});