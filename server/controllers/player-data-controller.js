/**
 * Provides CRUD operations for the player data
 * @author niall mcguinness 
 */

const db = require('./db');
const PlayerData = require('../models/player-data');

/**
 * Creates a record in the collection based on the JSON object using POST
 * @returns         Array of JSON objects or error
 * @method          POST
 * @url             http://127.0.0.1:3000/create
 * @example We use the code below with data and options set according to the action and what data we need to send
 * 
 * let theData = {
 *     game: "Split Screen Same", 
 *     player: "My Player Name", 
 *     score: 150
 * };
 * 
 * let options = {
 *     method: "POST",
 *     mode: "no-cors", 
 *     body: JSON.stringify(theData)
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.createOne = function (req, res) {

    showDebugInfo('Creating a record... ', req);
  
    let playerData = new PlayerData({
        game: req.body.game,
        team: req.body.team,
        wave: req.body.wave,
      //  newfield: req.body.newfield
    });

    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).insertOne(playerData, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to create a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Reads first JSON object in the collection with the ID specified in request URL path
 * @returns         Array of JSON objects or error
 * @method          GET
 * @url             http://127.0.0.1:3000/read
 * @example We use the code below with data and options set according to the action and what data we need to send
 *
 * let theData = {
 *     _id: "5ea80a60cc100d1a4ad5634c"
 * };
 * 
 * let options = {
 *     method: "GET",
 *     body: JSON.stringify(theData)
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.readOne = function (req, res) {

    console.log('Reading a record... ');
  
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).find({
        _id: db.getPrimaryKey(req.body._id)
    }).toArray((err, documents) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to read a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(documents);
        }
    });
}

/**
 * Reads and returns all the collection as an array of JSON objects
 * @returns         Array of JSON objects or error
 * @method          GET
 * @url             http://127.0.0.1:3000/read/all
 * @example We use the code below with data and options set according to the action and what data we need to send
 * 
 * let options = {
 *     method: "GET",
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.readAll = function (req, res) {

    console.log('Reading all records... ');
  
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).find({}).toArray((err, documents) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to read records',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(documents);
        }
    });
}


/**
 * Updates and returns the first JSON object in the collection matching the ID specified in the JSON object provided
 * @returns         Array of JSON objects or error
 * @method          PUT
 * @url             http://127.0.0.1:3000/update
 * @example We use the code below with data and options set according to the action and what data we need to send
 * 
 * let theData = {
 *     _id: "5ea80a60cc100d1a4ad5634c"
 *     game: "NAME GAME NAME HERE", 
 *     player: "NEW PLAYER NAME HERE", 
 *     score: 150
 * };
 * 
 * let options = {
 *     method: "PUT",
 *     body: JSON.stringify(theData)
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.updateOne = function (req, res) {

    console.log('Updating a record... ');
    console.log('req.body :>> ', req.body);
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).findOneAndUpdate({
        _id: db.getPrimaryKey(req.body._id)
    }, {
        $set: {
            game: req.body.game,
            player: req.body.player,
            score: req.body.score
        }
    }, {
        returnOriginal: false
    }, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to update a record',
                message: err.message
            });
        } else {
            //had some CORS errors from POSTMAN and browser early in the development and added this
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Deletes and returns the (newly deleted) first JSON object in the collection matching the ID specified in request URL path
 * @returns         Array of JSON objects or error
 * @method          DELETE
 * @url             http://127.0.0.1:3000/delete
 * @example We use the code below with data and options set according to the action and what data we need to send
 *
 * let theData = {
 *      _id: "5ea80a60cc100d1a4ad5634c", 
 * };
 * 
 * let options = {
 *     method: "DELETE", 
 *     body: JSON.stringify(theData)
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.deleteOne = function (req, res) {

    console.log('Deleting one record... ');
  
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).findOneAndDelete({
        _id: db.getPrimaryKey(req.body._id)
    }, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to delete a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Deletes multiple rows in the collection matching the IDs specified in request body
 * @returns         Array of JSON objects or error
 * @method          DELETE
 * @url             http://127.0.0.1:3000/delete/many
 * @example We use the code below with data and options set according to the action and what data we need to send
 * 
 * let theData = [
 *      "5ea80a60cc100d1a4ad5634c", 
 *          "5ea80a60cc100d1a4ad5634d"
 *              "5ea80a60cc100d1a4ad5634e"
 * ];
 * 
 * let options = {
 *     method: "DELETE", 
 *     body: theData
 * };
 * 
 * let gdRest = new GDREST();
 * gdRest.getData(url, options).then(data => {  
 *       //use data
 *   }).catch(error =>{
 *      //report error
 *   });
 */
exports.deleteMany = function (req, res) {

    console.log('Deleting many records... ');

    let ids = req.body.map(_id => db.getPrimaryKey(_id));
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).deleteMany({
        _id: {
            $in: ids
        }
    }, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to delete a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/************************************** UTILITY FUNCTIONS **************************************/
/**
 * Used to show the debug info (i.e. request contents) on an incoming request
 *
 * @param {string} strPrompt String prompt for debug info
 * @param {Object} req Incoming request object
 */
function showDebugInfo(strPrompt, req){
    console.log(strPrompt);
    console.log('req.headers :>> ', req.headers);
    console.log('req.body :>> ', req.body);
    console.log('req.url :>> ', req.url);
    console.log('req.method :>> ', req.method);
    console.log('req.route :>> ', req.route);
}

